"""
Simple color-based tracker. Uses masks in the HSV color space to isolate an
object. Then computes image moments to get the centroid.
Based on https://www.pyimagesearch.com/2015/09/14/ball-tracking-with-opencv/
"""


from imutils.video import VideoStream
import numpy as np
import cv2
import imutils
import time


class CVTracker():
    
    def __init__(self,
                 video_stream=2, # number of webcam (X in /dev/videoX)
                 color_lower=(0, 109, 186), # lower level of color mask (in HSV)
                 color_upper=(37, 255, 255), # upper level of color mask
                 calibration_scaling=13.8, # scale for calibration (pixels per cm
                 calibration_zero=np.array([83, 248], dtype=np.float32) # zero point for calib.
                ): 

        self._color_lower = color_lower
        self._color_upper = color_upper
        self._calibration_scaling = calibration_scaling
        self._calibration_zero = calibration_zero
        self.x = 0
        self.y = 0
        # self.w = 0
        # self.h = 0
        self.r = 0
        self._frame = None

        # Start getting video from camera
        self._video_stream =  VideoStream(src=video_stream).start()
        # allow the camera or video file to warm up
        time.sleep(2.0)

    def get_position(self):
        """ Gives the position of the tracked object

        Parameters:
        -----------------
        none
        Returns:
        -----------------
        center - x,y coordinates of the object - np.array of float32 ([x, y])
                 None if object could not be located or webcam doesn't work
        """
        # grab the current frame
        self.frame = self._video_stream.read()
 
        # resize the frame, blur it, and convert it to the HSV
        # color space
        self.frame = imutils.resize(self.frame, width=600)
        blurred = cv2.GaussianBlur(self.frame, (11, 11), 0)
        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
 
        # construct a mask for the color, then perform
        # a series of dilations and erosions to remove any small
        # blobs left in the mask
        mask = cv2.inRange(hsv, self._color_lower, self._color_upper)
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)


        # find contours in the mask and initialize the current
        # (x, y) center of the tracker
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
 
        # only proceed if at least one contour was found
        if len(cnts) > 0:
                # find the largest contour in the mask, then use
                # it to compute the minimum enclosing circle and
                # centroid
                c = max(cnts, key=cv2.contourArea)
                ((self.x, self.y), self.r) = cv2.minEnclosingCircle(c)
                M = cv2.moments(c)
                center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
 
                # only proceed if the radius meets a minimum size
                if self.r > 10:

                        return (np.array(center, dtype=np.float32) -
                                self._calibration_zero) / self._calibration_scaling
        return None

    def render(self): 
        # draw the bounding circle on the frame

        cv2.circle(self.frame, (int(self.x), int(self.y)), int(self.r),
                  (0, 255, 255), 2)

        # show the frame to our screen
        cv2.imshow("Frame", self.frame)
        key = cv2.waitKey(1) & 0xFF

## Stopping code (seems unnecessary with the way I'm using CV2)

    def __del__(self):
        self._video_stream.stop()
        time.sleep(3.0)
        cv2.destroyAllWindows()


