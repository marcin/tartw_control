"""
Simple control for pneumatics setup
"""

import pneuvironment
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
import numpy as np

msg_commands = """
vx - work on valve x
mx - modulate_valve with duty cycle x (0 - 4096)
px - set pressure setpoint to x (0 - 1.5)
t - toggle valve
? - display commands
"""

no_valves = 8
port = '/dev/ttyACM0'

class PlotAnimator():
    """ 
    For animating and plotting data in real time
    """
    def __init__(self, datafun, title, xlabel, interval=50, window=100,
                 yrange=[-0.1, 1.5]):
        self._datafun = datafun
        self._interval = interval
        self._frame = 0
        self._starttime = time.time()
        self._window = window
        self._fig = plt.figure()
        self._datasize = np.shape(datafun())[0]
        self._xs = np.arange(self._window)
        self._ys = np.zeros((self._datasize, self._window))
        self._axes = []
        self._lines = []
        for i in range(self._datasize):
            ax = self._fig.add_subplot(self._datasize, 1, i+1)
            ax.set_ylim(yrange)
            self._lines.append(ax.plot(self._xs, self._ys[i, :], 'o--')[0])
            self._axes.append(ax)
        plt.title = title
        plt.xlabel = xlabel
        

    def _animate_fun_slow(self, i, xs, ys):
        """
        To be passed to matplotlib animator
        """
        # Get new data
        pos_in_graph = self._frame % self._window
        xs[pos_in_graph] = time.time() - self._starttime
        ys[:, pos_in_graph] = self._datafun()

        # Discard data older than window

        for it, ax in enumerate(self._axes):
            ax.clear()
            ax.plot(xs, ys[it, :], 'o--')

        self._frame += 1

    def _animate_fun_fast(self, i, ys):
        pos_in_graph = self._frame % self._window
        ys[:, pos_in_graph] = self._datafun()
        for it, line in enumerate(self._lines):
            line.set_ydata(ys[it, :])
        self._frame += 1
        return line,


    def animate_slow(self):
        self._ani = animation.FuncAnimation(self._fig,
                                      self._animate_fun_slow,
                                      fargs=(self._xs, self._ys),
                                      interval=self._interval)
        plt.ion()
        plt.show()

    def animate_fast(self):
        self._ani = animation.FuncAnimation(self._fig, 
                                            self._animate_fun_fast,
                                            fargs=(self._ys,),
                                            interval=self._interval,
                                            blit=True)
        plt.ion()
        plt.show()



def main():
    # initialise environment
    pneuenv = pneuvironment.Pneuvironment(no_valves, port=port)
    
    print(msg_commands)
    # initialise plotting stuff
    datafun = lambda : np.concatenate((pneuenv.get_pressures(),
                                      pneuenv.get_accelerations(), 
                                      pneuenv.get_orientation()))
    animator = PlotAnimator(pneuenv.get_board_sensor_data, "Pressures in the system",
                            "Time (s)") 
    animator.animate_slow()
    valve = 0

    while True:
        command = input()

        if command[0] == 't':
            pneuenv.switch_valve(valve,
                                not pneuenv.valve_states[valve])
        if command[0] == 'm':
            pneuenv.modulate_valve(valve, int(command[1:]))

        if command[0] == 'p':
            pneuenv.set_valve_pressure(valve, float(command[1:]))

        if command[0] == 'v':
            valve = int(command[1:])
            print("Valve: %d" % valve)
        if command[0] == 'q':
            break


if __name__ == '__main__':
    main()



