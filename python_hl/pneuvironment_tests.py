""" 
Tests for pneumatics setup
"""
import pneuvironment
import unittest
import os
import numpy as np
import time

def get_confirmation(prompt):
    """
    Small function to get confirmation from user
    """
    while True:
        char = input(prompt)
        if char == "n":
            return False
        if char == "y":
            return True

class TestPneumatics(unittest.TestCase):

    port = "/dev/ttyACM0"
    no_valves = 8

    def setUp(self):
        self.pneuenv = pneuvironment.Pneuvironment(self.no_valves,
                                                   port=self.port,
                                                   render=True)


    def tearDown(self):
        del self.pneuenv

    def test_valve_actuation(self):
        while not get_confirmation("We're going to test valves. Ready? (y/n)"):
            pass
        for i in range(self.no_valves):
            self.pneuenv.switch_valve(i, True)
            if not get_confirmation("Did valve %d switch? (y/n)" % i):
                raise IOError("Couldn't actuate valve %d" %i)
            self.pneuenv.switch_valve(i, False)
        for i in range(self.no_valves):
            self.pneuenv.switch_valve(i, False)

    def test_pressure_reading(self):
        pressures = self.pneuenv.get_pressures()
        assert (len(pressures) == self.no_valves), "no. of pressure readings != no. \
        of valves"
        for i, pressure in enumerate(pressures):
            print("Pressure in sensor %d is %f" % (i, pressure))
            self.assertTrue((pressure > -0.1 and pressure < 10), 
                            "pressure reading out of range. Check sensor connection")

    def test_stepping(self):
        while not get_confirmation(
            "set up robot in starting position. set up? (y/n)"):
            pass

        cumtime = 0.0
        for i in range(100):
            actuation_pattern = np.array(self.no_valves * 
                                         [1 if (i%2 == 0) else 0])
            starttime = time.time()
            next_state, reward, done, _ = self.pneuenv.step(actuation_pattern)
            self.pneuenv.render()
            cumtime += time.time() - starttime

        print ("Average control frequency {0}".format(100/cumtime))
        print(next_state, reward, done)
        self.assertFalse(done, "Position reported out of range?")

        while not get_confirmation(
            "set up robot at the end of the track. set up? (y/n)"):
            pass

        next_state, reward, done, _ = self.pneuenv.step(np.array(self.no_valves * [0]))

        self.assertTrue(done, "Robot seems to be still on the track")

        print(next_state, reward, done)

if __name__ == '__main__':
    TestPneumatics.port = os.environ.get('port', TestPneumatics.port)            
    unittest.main()
