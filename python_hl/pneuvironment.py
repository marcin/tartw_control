"""
Environment for controlling a pneumatic robot in a Markov Decision Process
Framework. Mimics OpenAI's gym environments

"""
import PyCmdMessenger
import numpy as np
import utils
import cv_tracker
import time
import pyqtgraph as pg
import tensorflow as tf

import gym.spaces

# ---------------Constants--------------
# Pressure range in the systen
PRESSURE_MAX = 1.000 
PRESSURE_MIN = 0.0
# IMU readings max possible values for euler and quaternion (I'm using
# quaternion)
IMU_YAW_MAX = 360.0
IMU_YAW_MIN = 0.0
IMU_ROLL_MAX = 180.0
IMU_ROLL_MIN = -180.0
IMU_PITCH_MAX = 180.0
IMU_PITCH_MIN = -180.0
IMU_QUATERNION_MAX = 1.0
IMU_QUATERNION_MIN = -1.0
# When X or Y quaternion are outside of [-0.6, 0.6], the robot is on its side,
# so the environment should stop
IMU_QUATERNION_FALL_MAX = 0.6
IMU_QUATERNION_FALL_MIN = -0.6
# Dimensions of the field
POSITION_X_MIN = -10.0
POSITION_X_MAX = 35.0
POSITION_Y_MIN = -15.0
POSITION_Y_MAX = 15.0
# Velocity bounds
VELOCITY_MIN = float("-inf")
VELOCITY_MAX = float("inf")
# Constants for calculating return functions
Y_VELOCITY_COST_MULTIPLIER = 0.01
FINISH_REWARD = 500
LEVEL_PENALTY = 5


class Pneuvironment():

    def __init__(self,
                 no_valves, # number of valves on the board
                 duty_cycle_max = 4096, # maximum possible valve duty cycle
                 pressure_max = 1.8, # maximum possible pressure setpoint
                 port="/dev/ttyACM0", # serial port of the Arduino
                 baudrate=230400, # baudrate of the Arduino
                 webcam_no=2, # number of the webcam used for tracking
                 control_frequency = 7, # frequency of control updates (Hz)
                 render=False, # Set true if you'd like to render data
                 plot_history_size=100, # Number of points visible in the plot
                ):


        self._control_period = 1 / control_frequency

        ################################################################
        ### ----------- Fluidic board interface setup ---------------###
        ################################################################

        # Hardware parameters
        self._no_valves = no_valves
        self._duty_cycle_max = duty_cycle_max
        self._pressure_max = pressure_max

        # Serial comm parameters
        serial_commands = [
                           ["error", "si*"],
                           ["valve_switch_multiple", self._no_valves*"?"],
                           ["valve_switch_single", "i?"],
                           ["valve_modulate_single", "ii"],
                           ["valve_set_pressure", "if"],
                           ["request_pressure_readings", ""],
                           ["send_pressure_readings", self._no_valves*"f"],
                           ["request_accelerometer_readings", ""],
                           ["send_accelerometer_readings", 3*"f"],
                           ["request_orientation", ""],
                           ["send_orientation", 4*"f"],
                           ["request_all_sensor_data", ""],
                           ["send_all_sensor_data", (self._no_valves+4)*"f"],
                           ["get_IMU_calibration", ""],
                           ["send_IMU_calibration", 4*"i"],
                          ]

        self._arduino = PyCmdMessenger.ArduinoBoard(port,
                                                    baud_rate=baudrate,
                                                    float_bytes=4,
                                                    double_bytes=8,
                                                    int_bytes=4)

        self._cmd_messenger = PyCmdMessenger.CmdMessenger(self._arduino,
                                                         serial_commands)
        
        # Calibrate IMU
        self._calibrate_IMU()

        # Initialise all valves as switched off
        self._switch_multiple_valves([False]*self._no_valves)
        self.valve_states = [False]*self._no_valves

        ###################################################
        ### ----------- CV tracker setup ---------------###
        ###################################################

        self._tracker = cv_tracker.CVTracker(video_stream = webcam_no)

        ########################################################
        ### ----------- Gym environment setup ---------------###
        ########################################################

        observation_space_highs = np.array(self._no_valves * [PRESSURE_MAX] +
                                           4 * [IMU_QUATERNION_MAX] +
                                           [POSITION_X_MAX,
                                            POSITION_Y_MAX] +
                                           2 * [VELOCITY_MAX]
                                          )
        observation_space_lows = np.array(self._no_valves * [PRESSURE_MIN] +
                                           4 * [IMU_QUATERNION_MIN] +
                                           [POSITION_X_MIN,
                                            POSITION_Y_MIN] +
                                           2 * [VELOCITY_MIN]
                                         )
        self.observation_space = gym.spaces.Box(observation_space_lows,
                                                observation_space_highs,
                                                dtype=np.float32)

        self.action_space = gym.spaces.MultiBinary(self._no_valves)
        self._state_dim = self.observation_space.shape[0]

        # A few useful indices for later
        self._x_position_index = self._no_valves + 4
        self._y_position_index = self._no_valves + 5
        self._x_velocity_index = self._no_valves + 6
        self._y_velocity_index = self._no_valves + 7
        self._quaternion_x_index = self._no_valves + 1
        self._quaternion_y_index = self._no_valves + 2
        self._quaternion_indices = np.array([self._quaternion_x_index,
                                             self._quaternion_y_index])

        # Boundaries of the space
        self._position_max = np.array([POSITION_X_MAX - 5.0,
                                      POSITION_Y_MAX - 5.0])
        self._position_min = np.array([POSITION_X_MIN + 5.0,
                                      POSITION_Y_MIN + 5.0])
        self._quaternion_max = np.array([IMU_QUATERNION_FALL_MAX - 0.1,
                                         IMU_QUATERNION_FALL_MAX - 0.1])
        self._quaternion_min = np.array([IMU_QUATERNION_FALL_MIN + 0.1,
                                         IMU_QUATERNION_FALL_MIN + 0.1])
        
        # Remembering last state for plotting etc.
        self._last_action_time = time.time()
        self._last_state = np.zeros(self.observation_space.shape[0])
        self._last_positions = np.zeros(2)


        ######################################################
        ### ----------- Live plotting setup ---------------###
        ######################################################
        if render:
            # Plot data buffer
            self._plot_history_size = plot_history_size
            self._plot_data = np.zeros((self._state_dim,
                                        plot_history_size))
            # Plot window and layout
            self._plot_application = pg.Qt.QtGui.QApplication([])
            self._plot_view = pg.GraphicsView()
            self._plot_layout = pg.GraphicsLayout()
            self._plot_view.setCentralItem(self._plot_layout)
            self._plot_view.show()
            self._plot_view.resize(1000, 600)
            self._plot_view.setWindowTitle("Live pneuvironment data")

            pg.setConfigOptions(antialias=True)
            self._plots = []
            self._curves = []

            for i in range(self._no_valves):
                plot_title = "Valve {0}".format(i)
                self._plots.append(self._plot_layout.addPlot(title=plot_title,
                                   row=i, col=0))
                self._curves.append(self._plots[i].plot(np.zeros(self._state_dim)))
            

            plot_titles = ["Quaternion W", "Quaternion X", "Quaternion Y",
                           "Quaternion Z", "X position", "Y position",
                           "X velocity", "Y velocity"]

            for i, plot_title in enumerate(plot_titles):
                self._plots.append(self._plot_layout.addPlot(title=plot_title,
                                   row=i, col=1))
                self._curves.append(self._plots[i + self._no_valves]\
                                    .plot(np.zeros(self._state_dim)))

    def reset(self):
        """Puts the environment back in initial state
        Returns:
        -----------------
        state - state after reset - np.array of dim (, self.state_dim)"""
        self._switch_multiple_valves([False]*self._no_valves)
        utils.get_confirmation("Please put the environment in the initial state" 
                               " (y/n)")
        time.sleep(2.0)
        board_sensor_data = self.get_board_sensor_data()
        positions = self._tracker.get_position()
        while not (-1 < positions[0] < 1) and not (-1 < positions[1] < 1):
            utils.get_confirmation(("Robot position out of range ({0}, {1}). Please"
                                    " adjust it to start. Adjusted"
                                    " (y/n)").format(*positions))
            positions = self._tracker.get_position()

        velocities = np.array([0, 0])

        next_state = np.concatenate((board_sensor_data, positions, velocities))
        self._last_state = next_state

        return next_state

    def step(self, action):
        """
        Pushes environment one step forward

        Parameters:
        -----------------
        action - action to take (valve actuation pattern) - list of bool,
                                                            len=self._no_valves

        Returns:
        -----------------
        next_state - data for new state - numpy object of dim self._state_dim
        reward - reward from the transition - float
        done - true if in final state - boolean
        message - optional in OpenAI gym, says how the env terminated - str
        """

        # Wait until next control step
        time_interval = time.time() - self._last_action_time

        while (time_interval < self._control_period):
            time_interval = time.time() - self._last_action_time
            pass

        # Take action
        action_bool = np.array(action, dtype=np.bool)
        self._switch_multiple_valves(action_bool)
        self._last_action_time = time.time()
        
        # Wait a while to make sure that the measurement is a result of the
        # action taken

        time.sleep(0.05)

        # Read results of action
        board_sensor_data = self.get_board_sensor_data()
        positions = self._tracker.get_position()
        while positions is None:
            print("Can't get position\n")
            positions = self._tracker.get_position()
        velocities = (positions - self._last_positions) / time_interval
        self._last_positions = positions
        
        next_state = np.concatenate((board_sensor_data, positions, velocities))
        self._last_state = next_state

        # Get results and see if we're finished
        # Reward is proportional to velocity, + 1000 if you reached the finish
        # line)

        # reward = velocities[0] - np.abs(velocities[1]) * Y_VELOCITY_COST_MULTIPLIER + \
                 # (positions[0] > self._position_max[0]) * FINISH_REWARD
        reward = velocities[0]

        done_position = (np.any(positions > self._position_max) or
                         np.any(positions < self._position_min))
        done_orientation = (np.any(board_sensor_data[self._quaternion_indices] > 
                            self._quaternion_max) or 
                            np.any(board_sensor_data[self._quaternion_indices] <
                            self._quaternion_min))

        done = done_position or done_orientation

        message = None

        if done_position:
           message = 'position'
        elif done_orientation:
           message = 'orientation'

        return next_state, reward, done, message

    def cost_fn(self, states, actions, next_states):
        """Calculates the expected cost from a tensor of past states and next
        states

        Parameters:
        -----------------
        states - input states - tf tensor (?, state_dim)
        actions - action taken in states - tf tensor (?, action_dim)
        next_states - stated entered after taking action - tf tensor (?,
                      state_dim)

        Returns:
        -----------------
        scores - costs from each transitions - tf tensor (?, 1)
        """

        # expand states if feeding just one at a time
        states_shape = states.get_shape()
        is_single_state = (len(states_shape) == 1) 


        if is_single_state:
            states = states[None, ...]
            actions = actions[None, ...]
            next_states = next_states[None, ...]

        scores = tf.zeros(actions.get_shape()[0].value) 

        # scores += tf.abs(next_states[:, self._quaternion_x_index] *
                         # LEVEL_PENALTY)
        # scores += tf.abs(next_states[:, self._quaternion_y_index] *
                         # LEVEL_PENALTY)
        scores -= next_states[:, self._x_velocity_index]

        if is_single_state:
            scores = scores[0]

        return scores

    def render(self):
        self._tracker.render()
        self._plot_data =  np.roll(self._plot_data, 1, axis=1)
        self._plot_data[:, -1] = self._last_state

        for i in range(self._state_dim):
            self._curves[i].setData(self._plot_data[i, :])

        pg.QtGui.QApplication.processEvents()

    def get_pressures(self):
        """
        Reads pressures from the Arduino

        Parameters:
        -----------------------
        none

        Returns:
        -----------------------
        pressures - np.array of float of length self._no_valves
        """

        self._cmd_messenger.send("request_pressure_readings")
        msg = self._cmd_messenger.receive()
        return np.array(msg[1])

    def get_accelerations(self):
        """Reads accelerations from the Arduino

        Parameters:
        -----------------------
        none

        Returns:
        -----------------------
        accelerations - np.array of float of length 3
        """

        self._cmd_messenger.send("request_accelerometer_readings")
        msg = self._cmd_messenger.receive()
        return np.array(msg[1])

    def get_orientation(self):
        """
        Reads Euler orientation from the Arduino

        Parameters:
        -----------------------
        none

        Returns:
        -----------------------
        orientation - np.array of float of length 3
        """

        self._cmd_messenger.send("request_orientation")
        msg = self._cmd_messenger.receive()
        return np.array(msg[1])

    def get_board_sensor_data(self):
        """
        Reads all sensors from the Arduino

        Parameters:
        -----------------------
        none

        Returns:
        -----------------------
        state - np.array of float of length (no_valves + 6,) 
                state[0:no_valves - 1] - pressure in each channel
                state[no_valves : no_valves + 2] - linear accelerations
                (acceleration without gravity)
                state[no_valves + 3 : no_valves + 5] - orientation in space in
                euler coordinates
        """

        self._cmd_messenger.send("request_all_sensor_data")
        return np.array(self._cmd_messenger.receive()[1]) 

    def switch_valve(self, valve_no, state):
        """
        Switches a single valve

        Parameters:
        ----------------------
        valve_no - number of valve to switch - int [0, no_valves) 
        state - state to which valve should be switched - boolean

        Returns:
        ----------------------
        none
        """
        assert (valve_no < self._no_valves), "Valve number out of range"
        self._cmd_messenger.send("valve_switch_single", valve_no, state)
        self.valve_states[valve_no] = state

    def _switch_multiple_valves(self, states):
        """
        Switches all valves 

        Parameters:
        ----------------------
        states - list of states for all valves - list of bool,
                                          len=self._no_valves

        Returns:
        ----------------------
        none
        """
        self._cmd_messenger.send("valve_switch_multiple", *states)
        self.valve_states= states

    def modulate_valve(self, valve_no, duty_cycle):
        """
        Modulates a single valve

        Parameters:
        ----------------------
        valve_no - number of valve to switch - int [0, no_valves) 
        duty_cycle - desired duty cycle - int

        Returns:
        ----------------------
        none
        """ 
        assert (valve_no < self._no_valves), "Valve number out of range"
        assert (duty_cycle < self._duty_cycle_max and
                duty_cycle >= 0), "Duty cycle out of range"

        self._cmd_messenger.send("valve_modulate_single", valve_no, duty_cycle)
        self.valve_states[valve_no] = duty_cycle

    def set_valve_pressure(self, valve_no, pressure):
        """
        Sends a pressure setpoint to the controller

        Parameters:
        ----------------------
        valve_no - number of valve - int [0, no_valves) 
        pressure - pressure setpoint - float

        Returns:
        ----------------------
        none
        """ 
        assert (valve_no < self._no_valves and
                valve_no >= 0), "Valve number out of range"
        assert (pressure < self._pressure_max and
                pressure >= 0), "Pressure setpoint out of range"

        self._cmd_messenger.send("valve_set_pressure", valve_no, pressure)

    def _calibrate_IMU(self):
        """
        Makes sure that the IMU is calibrated. Uncalibrated IMU could results
        in bad quality orientation data

        Parameters:
        ----------------------
        none

        Returns:
        ----------------------
        none
        """

        while True:
            time.sleep(1)
            self._cmd_messenger.send("get_IMU_calibration")
            msg = self._cmd_messenger.receive()

            if msg==None:
                print(msg)
                continue
            elif msg[0] == 'error':
                print(msg)
            elif not all(i>2 for i in msg[1]):
                print(("Please calibrate IMU. System {0}, Gyro {1},"
                       "Accel {2},Mag {3}").format(*msg[1]))
            else:
                print(msg)
                break
