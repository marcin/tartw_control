""" 
Tests for cv_tracking setup
"""
import cv_tracker
import unittest
import numpy as np
import os
import time

def get_confirmation(prompt):
    """
    Small function to get confirmation from user
    """
    while True:
        char = input(prompt)
        if char == "n":
            return False
        if char == "y":
            return True

class TestTracking(unittest.TestCase):

    def setUp(self):
        self.tracker = cv_tracker.CVTracker()

    def tearDown(self):
        pass

    def test_tracking(self):
        for i in range(1000):
            starttime = time.time()
            center = self.tracker.get_position()
            print('FPS:', 1 / (time.time() - starttime))
            self.tracker.render()
            print('Position', center)
            self.assertTrue(center is not None and 
                            (center[0] >= -1.0) and 
                            (center[1] > -100.0 and center[1] < 100.0),
                            "Object center given ({0}) is None or out of \
                            range".format(center))

if __name__ == '__main__':
    unittest.main()
