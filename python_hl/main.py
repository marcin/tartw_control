""" Main control function """

import os
import argparse
import time

import pneuvironment
from logger import logger
from model_based_rl import ModelBasedRL
# from half_cheetah_env import HalfCheetahEnv

# Parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('action', type=str, choices=('evaluate_model',
                                                 'run_full_algorithm'))
parser.add_argument('--exp_name', type=str, default=None)
parser.add_argument('--env', type=str, default='pneuvironment',
                    choices=('pneuvironment, HalfCheetah',))
parser.add_argument('--render', action='store_true')
parser.add_argument('--mpc_horizon', type=int, default=25)
parser.add_argument('--num_random_action_selection', type=int, default=4096)
parser.add_argument('--nn_layers', type=int, default=1)
parser.add_argument('--hidden_dim', type=int, default=500)
parser.add_argument('--num_training_epochs', type=int, default=1000)
parser.add_argument('--num_onpolicy_rollouts', type=int, default=10)
parser.add_argument('--num_init_random_rollouts', type=int, default=10)
parser.add_argument('--num_onplicy_iters', type=int, default=5)
parser.add_argument('--max_rollout_length', type=int, default=300)
parser.add_argument('--no_valves', type=int, default=8)
parser.add_argument('--port', type=str, default='/dev/ttyACM0')
parser.add_argument('--random_dataset', type=str, default=None)
parser.add_argument('--init_dataset', type=str, default=None)
parser.add_argument('--control_frequency', type=int, default=3)

args = parser.parse_args()

# Establish folders for logging data
data_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data')
exp_name = '{0}_{1}_{2}'.format(args.env,
                                args.action,
                                args.exp_name if args.exp_name else time.strftime("%d-%m-%Y_%H-%M-%S"))
exp_dir = os.path.join(data_dir, exp_name)
assert not os.path.exists(exp_dir),\
    'Experiment directory {0} already exists. Either delete the directory, or run the experiment with a different name'.format(exp_dir)
os.makedirs(exp_dir, exist_ok=True)
logger.setup(exp_name, os.path.join(exp_dir, 'log.txt'), 'debug')

# Set up test environment

env = {
    'pneuvironment': pneuvironment.Pneuvironment(args.no_valves,
                                                 port=args.port,
                                                 render=args.render,
                                                 webcam_no=2,
                                                 control_frequency=args.control_frequency),
    # 'HalfCheetah': HalfCheetahEnv()
}[args.env]

mbrl = ModelBasedRL(env=env,
                    render=args.render,
                    mpc_horizon=args.mpc_horizon,
                    num_random_action_selection=args.num_random_action_selection,
                    nn_layers=args.nn_layers,
                    training_epochs=args.num_training_epochs,
                    num_onpolicy_rollouts=args.num_onpolicy_rollouts,
                    num_onplicy_iters=args.num_onplicy_iters,
                    num_init_random_rollouts=args.num_init_random_rollouts,
                    dataset_dump_dir=exp_dir,
                    random_dataset_dir=args.random_dataset,
                    init_dataset_dir=args.init_dataset,
                    max_rollout_length=args.max_rollout_length,
                    hidden_nn_dim=args.hidden_dim)

# Run 

run_func = {
    'evaluate_model': mbrl.evaluate_nn_model,
    'run_full_algorithm': mbrl.run_full_algorithm,
}[args.action]

run_func()

