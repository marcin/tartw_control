import pneuvironment
import numpy as np
import model_based_rl
import utils
import pickle
from logger import logger
import os

env = pneuvironment.Pneuvironment(8,render=True, control_frequency=2.86)
logger.setup('test_experiment', os.path.join('logs', 'log.txt'), 'debug')
mbrl = model_based_rl.ModelBasedRL(env, render=True,
                                   max_rollout_length=100,
                                   random_dataset_dir='./data/pneuvironment_test_model_random_collection/random_dataset.pkl')
policy = utils.AdamPolicy(env)

dataset = mbrl._gather_rollouts(policy, 5)

env.reset()


with open(os.path.join('data', 'adam_validation_set.pkl'), 'wb') as f:
    pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)
