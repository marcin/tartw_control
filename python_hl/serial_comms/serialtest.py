"""
Testing the serial protocol
"""
import serial_protocol
import time
import cProfile

PORT = '/dev/ttyACM1'
BAUDRATE = 230400
# ------------------- Commands --------------------------------------------
COMMANDS = {'actuate_valve': 0x01, 'read_sensor': 0x02,
            'acknowledge': 0xFF}


def main():
    serial = serial_protocol.MicroSerial(PORT, BAUDRATE)
    time.sleep(1)
    profiler = cProfile.Profile()
    msg = [1, 2, 3, 0x7d]*16
    profiler.enable()
    serial.write(msg)
    msg_received = serial.read()
    profiler.disable()
    print(msg_received == list(msg))
    profiler.print_stats()

if __name__ == "__main__":
    main()
