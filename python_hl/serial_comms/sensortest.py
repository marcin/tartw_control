"""
Tests the CmdMessager protocol
"""

import PyCmdMessenger
import time
import matplotlib.pyplot as plt

PORT = '/dev/ttyACM0'
BAUDRATE = 230400
NO_VALVES = 8
NO_SENSORS = 8 

COMMANDS  = [["error", "s"],
               ["valve_switch_multiple", NO_VALVES*"?"],
               ["valve_switch_single", "i?"],
               ["request_pressure_readings", ""],
               ["send_pressure_readings", NO_SENSORS*"f"]]

def main():
    arduino = PyCmdMessenger.ArduinoBoard(PORT, baud_rate=BAUDRATE, float_bytes=4,
                                          double_bytes=8, int_bytes=4)
    cmd_messenger = PyCmdMessenger.CmdMessenger(arduino, COMMANDS)
    vals = [True]* NO_VALVES 
    starttime = time.time()
    #plt.axis([0, 60, 0, 0.2])
    cumtime = 0
    for i in range(1000):
        looptime = time.time()
        vals = [not val for val in vals]
        cmd_messenger.send("valve_switch_multiple", *vals)
        cmd_messenger.send("request_pressure_readings")
        msg = cmd_messenger.receive()
        print(msg)
        cumtime += time.time() - looptime
        # time.sleep(0.003)
        time.sleep(1)
        
        # if (msg):
        #     plt.scatter(time.time() - starttime, msg[1], c="black")
        # plt.pause(0.01)
    print(cumtime/1000)

if __name__ == "__main__":
    main()
