"""
Serial protocol for sending stuff to and from a uC

The frame is very similar to PPP
(https://en.wikipedia.org/wiki/Point-to-Point_Protocol).

------------------------------------
| START_MARKER | BYTES | END_MARKER|
------------------------------------

Start and end markers are escaped with ESCAPE_MARKER. The char after escape
marker is XORed with XOR_NUM.

"""
import serial
import time
import warnings


# -------------------- Other constants -------------------------------
HANDSHAKE_TIMEOUT = 15
READ_TIMEOUT = 1

def coroutine(func):
    """
    A little decorator to use coroutines for parsing
    See: http://www.dabeaz.com/coroutines/Coroutines.pdf
    """
    def start(*args, **kwargs):
        cr = func(*args, **kwargs)
        cr.__next__()
        return cr
    return start


class MicroSerial:

    def __init__(self,
                 # -------------------- Serial port ---------------------------
                 port,
                 baudrate=9600,
                 float_bytes=4,
                 # ------------------- Character definitions ------------------
                 start_marker=0x7E,
                 end_marker=0x7E,
                 escape_marker=0x7D,
                 xor_char=0x20,
                 timeout=0,
                 ):
        """
        Creates the object, defines message format,
        initializes the connection
        """
        # Define constants
        self.start_marker = start_marker
        self.end_marker = end_marker
        self.escape_marker = escape_marker
        self.special_markers = set([start_marker, end_marker, escape_marker])
        self.xor_num = 0x20
        self.float_bytes = float_bytes



        # Open serial port
        self.serial_instance = serial.Serial(port, baudrate, timeout=timeout)
        self.serial_instance.flushInput()
        self.serial_instance.flushOutput()

        # -------------------- Do a handshake with the Arduino --------------

        # self.handshake()

        # Now check if everything's all right on the Arduino side


    def write(self, data=0):
        """
        Writes data to serial port

        Parameters:
        ----------------------------------------
            command: string
                Command from the command list
            data: bytes
                data to be sent

        Returns:
        --------------------------
            None
        """

        packet = self._frame_packet(data)
        print("packet: ", packet)
        self.serial_instance.write(packet)

    def _frame_packet(self, data):
        """
        Put data to be sent into a protocol packet.


        Parameters:
        ----------------------------------------
            data: bytes
                data to be sent
        Return:
        ----------------------------------------
            packet: bytes
                Packet to be sent over serial
        """
        # check if there's a start or end marker in data and insert special chars
        # in the right position

        # convert bytestream to list
        data = list(data)

        # build new list with escape markers in the right places
        packed_data = [self.start_marker]

        for byte in data:
            if byte in self.special_markers:
                packed_data.extend([self.escape_marker, byte])
            else:
                packed_data.append(byte)

        packed_data.append(self.end_marker)
        # Wrap the packet and return it
        return bytes(packed_data)

    def read(self):
        """
        Reads data from serial port

        Parameters:
        --------------------------
        None

        Returns:
        --------------------------
        data: bytes
            data received
        """
        # Reading is done through a pipeline built from coroutines
        # read() ---> unpacker ---> extract_data

        self.done_read = False
        unpacker = self._unpack_message()

        # Until the last function in the pipeline has stopped processing:
        while not self.done_read:
            nobytes = self.serial_instance.in_waiting
            if nobytes: 
                msg = self.serial_instance.read(999)
            #print(time.time()-timestart)
                for byte in msg:
                    unpacker.send(byte)

        return self.last_msg



    @coroutine
    def _unpack_message(self):
        """
        Unpacks message to extract command and data files

        Parameters:
        ------------------------
            message: bytes
                message to unpack
        Returns:
        ------------------------
            command: int
                command byte received
            data: bytes
                data received
        """
        # define next step in processing pipeline
        target = self._frame_receiver()
        # Outer loop looking for start marker
        while True:
            byte = (yield)
            frame = []

            if byte == self.start_marker:
                # When header found, read until end or MAX_MSG
                while True:
                    byte = (yield)
                    if byte == self.end_marker:
                        target.send(frame)
                        break
                    elif byte == self.escape_marker:
                        byte = (yield)
                        frame += [byte]
                    else:
                        frame += [byte]

    @coroutine
    def _frame_receiver(self):
        """
        A simple co-routine "sink" for getting frames
        """
        while True:
            frame = (yield)
            self.last_msg = frame
            self.done_read = True

    def handshake(self):
        """
        Execute a handshake between the Arduino and the host
        Parameters:
        --------------------------
            None

        Returns:
        --------------------------
            True if successful
        Raises:
        --------------------------
            SerialException
        """

        # The Arduino may restart after we open a serial port.
        # Wait for it to come up to speeed

        # It may die in the process, so let's count time to know when to stop
        starttime = time.time()
        handshake_complete = False

        while not handshake_complete:
            # Ask for a handshake
            self.write(self.commands['hello'])
            # Wait for reply
            # Check if there are any characters in the buffer
            while self.serial_instance.inWaiting() == 0:
                # If it's taking too long, something's wrong
                if (time.time() - starttime) > HANDSHAKE_TIMEOUT:
                    raise serial.SerialException('Timed out waiting for \
                                                  Arduino to restart')

            # When there is stuff in the buffer, read it
            command, data = self.read()

            # If the command is correct, the handshake is complete
            if command == self.commands['hello']:
                handshake_complete = True

        return True

    def close(self):
        """
        Disconnects from Arduino and closes serial instance
        """
        self.serial_instance.close()

    def __exit__(self):
        """
        Closes serial port
        """
        self.close()

    def printline(self):
        print(self.serial_instance.readline())
