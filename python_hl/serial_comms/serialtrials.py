import serial
from time import sleep, time


port = '/dev/pts/4'
ser = serial.Serial(port, 38400, timeout=0)

while True:
    time1 = time()
    time()
    time2 = time()
    print("time meas.", time2 - time1)

    if ser.in_waiting:
        starttime = time()
        data = ser.read(9999)
        endtime = time()
        print('Got', data, 'time', starttime-endtime)
    sleep(0.5)
ser.close()
