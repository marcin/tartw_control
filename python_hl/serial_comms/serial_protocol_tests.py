"""
Tests for the serial protocol using the unittest framework
"""

import unittest
import serial_protocol as sp
import serial
import time

class TestFraming(unittest.TestCase):

    def setUp(self):
        self.ser = sp.MicroSerial('/dev/pts/1', 230400,
                                  start_marker=ord('s'), end_marker=ord('e'),
                                  escape_marker=ord('_'))
        self.dummyser = serial.Serial('/dev/pts/2', 230400)
        self.time_start = time.time()

    def tearDown(self):
        exec_time = time.time() - self.time_start
        print("time for " + self.id() + " :" + str(exec_time))
        self.ser.close()

    def test_framing(self):
        """
        Tests the framing function by giving it special markers
        """
        sm = self.ser.start_marker
        em = self.ser.end_marker
        esc = self.ser.escape_marker
        testdict = {
            "start marker test": (bytes([sm]), bytes([sm, esc, sm, em])),
            "end marker test": (bytes([em]), bytes([sm, esc, em, em])),
            "escape_marker_test": (bytes([esc]), bytes([sm, esc, esc, em])),
            "all_marker_test": (bytes([sm, em, esc, 0x00, 0x01, 0x02]),
                                bytes([sm, esc, sm, esc, em, esc, esc, 0x00,
                                       0x01, 0x02, em]))
        }

        for testname, testval in testdict.items():
            self.assertEqual(self.ser._frame_packet(testval[0]), testval[1],
                             testname + " failed")

    def test_receiving(self):
        """
        Tests the receiving and parsing cmds
        """

        msg = [self.ser.start_marker, self.ser.end_marker,
               self.ser.escape_marker, 0, 1, 2]*1000
        msg_encoded = self.ser._frame_packet(bytes(msg))
        self.dummyser.write(msg_encoded)
        msg_received = self.ser.read()

        self.assertEqual(msg_received, msg, "sent packet not the \
                         same as received")




if __name__ == '__main__':
    unittest.main()
