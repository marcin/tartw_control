import os
import numpy as np
import matplotlib.pyplot as plt
import pickle
from model_based_policy import ModelBasedPolicy
import utils
from logger import logger
from timer import timeit
import time
import warnings


class ModelBasedRL(object):

    def __init__(self,
                 env,
                 num_init_random_rollouts=10,
                 max_rollout_length=500,
                 num_onplicy_iters=10,
                 num_onpolicy_rollouts=10,
                 training_epochs=1200,
                 training_batch_size=512,
                 render=False,
                 mpc_horizon=15,
                 num_random_action_selection=4096,
                 dataset_dump_dir='./datasets',
                 random_dataset_dir=None,
                 init_dataset_dir=None,
                 nn_layers=1,
                 hidden_nn_dim=500):
        self._env = env
        self._max_rollout_length = max_rollout_length
        self._num_onpolicy_iters = num_onplicy_iters
        self._num_onpolicy_rollouts = num_onpolicy_rollouts
        self._training_epochs = training_epochs
        self._training_batch_size = training_batch_size
        self._render = render
        self._dataset_dir = dataset_dump_dir

        if random_dataset_dir is None:
            logger.info('Gathering random dataset')
            self._random_dataset = self._gather_rollouts(utils.RandomPolicy(env),
                                                        num_init_random_rollouts)

            self._save_dataset(self._random_dataset, 'random_dataset')

        else:
            logger.info('Read random dataset from file')
            with open(random_dataset_dir, 'rb') as f:
                self._random_dataset = pickle.load(f)

        if init_dataset_dir is None:
            policy_init_dataset = self._random_dataset
            self._dataset = self._random_dataset

        else:
            logger.info('Read init dataset from file')
            with open(init_dataset_dir, 'rb') as f:
                dataset = pickle.load(f)
                policy_init_dataset = dataset
                self._dataset = dataset

        logger.info('Creating policy')
        self._policy = ModelBasedPolicy(env,
                                        policy_init_dataset,
                                        horizon=mpc_horizon,
                                        num_random_action_selection=num_random_action_selection, 
                                        nn_layers=nn_layers,
                                        hidden_dim=hidden_nn_dim)

        timeit.reset()
        timeit.start('total') 

    def _gather_rollouts(self, policy, num_rollouts):
        dataset = utils.Dataset()

        for rollout in range(num_rollouts):
            state = self._env.reset()
            done = False
            t = 0
            while not done:
                starttime = time.time()
                print("rollout: %d time: %d" % (rollout,t), end='\r')
                if self._render:
                    timeit.start('render')
                    self._env.render()
                    timeit.stop('render')
                timeit.start('get action')
                action = policy.get_action(state)
                timeit.stop('get action')
                # print(action)
                timeit.start('env step')
                next_state, reward, done, msg = self._env.step(action)

                if msg:
                    print(msg)

                timeit.stop('env step')
                done = done or (t >= self._max_rollout_length)
                dataset.add(state, action, next_state, reward, done)
                state = next_state
                t += 1
                runtime = time.time() - starttime
                print("%f Hz " % (1.0/(runtime)), end="")
                if runtime > 0.5:
                    warnings.warn("Step took too long", RuntimeWarning)

        self._env.reset()

        return dataset

    def _train_policy(self, dataset):
        """
        Train the model-based policy

        implementation details:
            (a) Train for self._training_epochs number of epochs
            (b) The dataset.random_iterator(...)  method will iterate through the dataset once in a random order
            (c) Use self._training_batch_size for iterating through the dataset
            (d) Keep track of the loss values by appending them to the losses array
        """
        timeit.start('train policy')

        losses = []
        ### PROBLEM 1
        ### YOUR CODE HERE
        for epoch in range(self._training_epochs):
            print("epoch: %d" % epoch, end="\r")
            for states, actions, next_states, _, _ in \
                                dataset.random_iterator(self._training_batch_size):
                loss = self._policy.train_step(states, actions, next_states)
                losses.append(loss)

        logger.record_tabular('TrainingLossStart', losses[0])
        logger.record_tabular('TrainingLossFinal', losses[-1])

        timeit.stop('train policy')

    def _log(self, dataset):
        timeit.stop('total')
        dataset.log()
        logger.dump_tabular(print_func=logger.info)
        logger.debug('')
        for line in str(timeit).split('\n'):
            logger.debug(line)
        timeit.reset()
        timeit.start('total')

    def evaluate_nn_model(self):
        """
        Train on a dataset, and see how good the learned dynamics model's predictions are.

        implementation details:
            (i) Train using the self._random_dataset
            (ii) For each rollout, use the initial state and all actions to predict the future states.
                 Store these predicted states in the pred_states list.
                 NOTE: you should *not* be using any of the states in states[1:]. Only use states[0]
            (iii) After predicting the future states, we have provided plotting code that plots the actual vs
                  predicted states and saves these to the experiment's folder. You do not need to modify this code.
        """
        logger.info('Training policy....')
        ### PROBLEM 1
        ### YOUR CODE HERE
        self._train_policy(self._dataset)
        logger.info('Evaluating predictions...')
        total_loss = self.evaluate_model()
        print("Total loss: {0}".format(total_loss))

    def evaluate_model(self):
        """  Evaluate current dynamics model  """
        mse = 0
        titles = ["Pressure 0", "Pressure 1", "Pressure 2", "Pressure 3",
                  "Pressure 4", "Pressure 5", "Pressure 6", "Pressure 7",
                  "Orientation W", "Orientation X", "Orientation Y", "Orientation Z",
                  "Position X", "Position Y", "Velocity X", "Velocity Y"]
        for r_num, (states, actions, _, _, _) in enumerate(self._random_dataset.rollout_iterator()):
            states = states[0:100]
            actions = actions[0:100]
            pred_states = []

            state_current = states[0]
            for action in actions:
                pred_states.append(state_current)
                state_current = self._policy.predict(state_current, action)

            states = np.asarray(states)
            pred_states = np.asarray(pred_states)

            mse += (np.square(states - pred_states)).mean(axis=None)

            state_dim = states.shape[1]
            rows = int(np.sqrt(state_dim))
            cols = state_dim // rows
            f, axes = plt.subplots(rows, cols, figsize=(3*cols, 3*rows))
            # f.suptitle('Model predictions (blue) versus ground truth (black) for open-loop predictions')
            for i, (ax, state_i, pred_state_i) in enumerate(zip(axes.ravel(), states.T, pred_states.T)):
                # ax.set_title('state {0}'.format(i))
                ax.set_title(titles[i])
                ax.plot(state_i, color='k')
                ax.plot(pred_state_i, color='#2b51db')
            plt.tight_layout()
            plt.subplots_adjust(top=0.90)
            f.savefig(os.path.join(logger.dir, 'prediction_{0:03d}.jpg'.format(r_num)), bbox_inches='tight')

        mse = mse / r_num

        logger.record_tabular('Eval loss', mse) 
        self._log(self._random_dataset)
        logger.info('All plots saved to folder')
        
        return mse

    def run_full_algorithm(self):
         """
         Starting with the random dataset, train the policy on the dataset, gather rollouts with the policy,
         append the new rollouts to the existing dataset, and repeat
         """

         itr = -1
         logger.info('Iteration {0}'.format(itr))
         logger.record_tabular('Itr', itr)
         self._log(self._dataset)

         for itr in range(self._num_onpolicy_iters + 1):
             logger.info('Iteration {0}'.format(itr))
             logger.record_tabular('Itr', itr)

             ### PROBLEM 3
             ### YOUR CODE HERE
             logger.info('Training policy...')
             self._train_policy(self._dataset)

             ### PROBLEM 3
             ### YOUR CODE HERE
             logger.info('Gathering rollouts...')
             new_dataset = self._gather_rollouts(self._policy, self._num_onpolicy_rollouts)

            ### PROBLEM 3
            ### YOUR CODE HERE
             logger.info('Appending dataset...')
             self._dataset.append(new_dataset)

             self._log(new_dataset)
             self._save_dataset(self._dataset, 'run_dataset_iter_' + str(itr))

    def _save_dataset(self, dataset, name):
        path =  os.path.join(self._dataset_dir, name + '.pkl')
        with open(path, 'wb') as f:
            pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)
