03-24 21:49:45 pneuvironment_run_policy_run_final_1 INFO     Read random dataset from file
03-24 21:49:46 pneuvironment_run_policy_run_final_1 INFO     Creating policy
03-24 21:49:47 pneuvironment_run_policy_run_final_1 INFO     Iteration -1
03-24 21:49:47 pneuvironment_run_policy_run_final_1 INFO     ---------  ---------
03-24 21:49:47 pneuvironment_run_policy_run_final_1 INFO     Itr         -1
03-24 21:49:47 pneuvironment_run_policy_run_final_1 INFO     ReturnAvg  -10.199
03-24 21:49:47 pneuvironment_run_policy_run_final_1 INFO     ReturnMax   -3.29004
03-24 21:49:47 pneuvironment_run_policy_run_final_1 INFO     ReturnMin  -14.3265
03-24 21:49:47 pneuvironment_run_policy_run_final_1 INFO     ReturnStd    3.91457
03-24 21:49:47 pneuvironment_run_policy_run_final_1 INFO     ---------  ---------
03-24 21:49:47 pneuvironment_run_policy_run_final_1 DEBUG    
03-24 21:49:47 pneuvironment_run_policy_run_final_1 DEBUG    : total      0.0 (100.0%)
03-24 21:49:47 pneuvironment_run_policy_run_final_1 DEBUG    : other      0.0 (100.0%)
03-24 21:49:47 pneuvironment_run_policy_run_final_1 DEBUG    
03-24 21:49:47 pneuvironment_run_policy_run_final_1 INFO     Iteration 0
03-24 21:49:47 pneuvironment_run_policy_run_final_1 INFO     Training policy...
03-24 21:50:14 pneuvironment_run_policy_run_final_1 INFO     Gathering rollouts...
03-24 22:04:16 pneuvironment_run_policy_run_final_1 INFO     Appending dataset...
03-24 22:04:16 pneuvironment_run_policy_run_final_1 INFO     -----------------  ----------------
03-24 22:04:16 pneuvironment_run_policy_run_final_1 INFO     Itr                     0
03-24 22:04:16 pneuvironment_run_policy_run_final_1 INFO     ReturnAvg              -9.52254
03-24 22:04:16 pneuvironment_run_policy_run_final_1 INFO     ReturnMax              -3.435
03-24 22:04:16 pneuvironment_run_policy_run_final_1 INFO     ReturnMin             -13.5107
03-24 22:04:16 pneuvironment_run_policy_run_final_1 INFO     ReturnStd               3.0057
03-24 22:04:16 pneuvironment_run_policy_run_final_1 INFO     TrainingLossFinal  589273
03-24 22:04:16 pneuvironment_run_policy_run_final_1 INFO     TrainingLossStart       3.36967e+06
03-24 22:04:16 pneuvironment_run_policy_run_final_1 INFO     -----------------  ----------------
03-24 22:04:16 pneuvironment_run_policy_run_final_1 DEBUG    
03-24 22:04:16 pneuvironment_run_policy_run_final_1 DEBUG    : total      869.2 (100.0%)
03-24 22:04:16 pneuvironment_run_policy_run_final_1 DEBUG    : get action 522.8 (60.2%)
03-24 22:04:16 pneuvironment_run_policy_run_final_1 DEBUG    : env step   176.7 (20.3%)
03-24 22:04:16 pneuvironment_run_policy_run_final_1 DEBUG    : train policy 26.8 (3.1%)
03-24 22:04:16 pneuvironment_run_policy_run_final_1 DEBUG    : other      142.8 (16.4%)
03-24 22:04:16 pneuvironment_run_policy_run_final_1 DEBUG    
03-24 22:04:16 pneuvironment_run_policy_run_final_1 INFO     Iteration 1
03-24 22:04:16 pneuvironment_run_policy_run_final_1 INFO     Training policy...
03-24 22:05:05 pneuvironment_run_policy_run_final_1 INFO     Gathering rollouts...
03-24 22:18:55 pneuvironment_run_policy_run_final_1 INFO     Appending dataset...
03-24 22:18:55 pneuvironment_run_policy_run_final_1 INFO     -----------------  ------------
03-24 22:18:55 pneuvironment_run_policy_run_final_1 INFO     Itr                     1
03-24 22:18:55 pneuvironment_run_policy_run_final_1 INFO     ReturnAvg              -5.95332
03-24 22:18:55 pneuvironment_run_policy_run_final_1 INFO     ReturnMax               3.17369
03-24 22:18:55 pneuvironment_run_policy_run_final_1 INFO     ReturnMin             -15.1448
03-24 22:18:55 pneuvironment_run_policy_run_final_1 INFO     ReturnStd               6.33884
03-24 22:18:55 pneuvironment_run_policy_run_final_1 INFO     TrainingLossFinal  454301
03-24 22:18:55 pneuvironment_run_policy_run_final_1 INFO     TrainingLossStart  824853
03-24 22:18:55 pneuvironment_run_policy_run_final_1 INFO     -----------------  ------------
03-24 22:18:55 pneuvironment_run_policy_run_final_1 DEBUG    
03-24 22:18:55 pneuvironment_run_policy_run_final_1 DEBUG    : total      879.1 (100.0%)
03-24 22:18:55 pneuvironment_run_policy_run_final_1 DEBUG    : get action 546.2 (62.1%)
03-24 22:18:55 pneuvironment_run_policy_run_final_1 DEBUG    : env step   197.3 (22.4%)
03-24 22:18:55 pneuvironment_run_policy_run_final_1 DEBUG    : train policy 49.1 (5.6%)
03-24 22:18:55 pneuvironment_run_policy_run_final_1 DEBUG    : other      86.5 (9.8%)
03-24 22:18:55 pneuvironment_run_policy_run_final_1 DEBUG    
03-24 22:18:55 pneuvironment_run_policy_run_final_1 INFO     Iteration 2
03-24 22:18:55 pneuvironment_run_policy_run_final_1 INFO     Training policy...
03-24 22:20:06 pneuvironment_run_policy_run_final_1 INFO     Gathering rollouts...
03-25 00:07:26 pneuvironment_run_policy_run_final_1 INFO     Appending dataset...
03-25 00:07:26 pneuvironment_run_policy_run_final_1 INFO     -----------------  ------------
03-25 00:07:26 pneuvironment_run_policy_run_final_1 INFO     Itr                     2
03-25 00:07:26 pneuvironment_run_policy_run_final_1 INFO     ReturnAvg               6.08673
03-25 00:07:26 pneuvironment_run_policy_run_final_1 INFO     ReturnMax              15.6546
03-25 00:07:26 pneuvironment_run_policy_run_final_1 INFO     ReturnMin              -1.90619
03-25 00:07:26 pneuvironment_run_policy_run_final_1 INFO     ReturnStd               5.20063
03-25 00:07:26 pneuvironment_run_policy_run_final_1 INFO     TrainingLossFinal  490022
03-25 00:07:26 pneuvironment_run_policy_run_final_1 INFO     TrainingLossStart  756217
03-25 00:07:26 pneuvironment_run_policy_run_final_1 INFO     -----------------  ------------
03-25 00:07:26 pneuvironment_run_policy_run_final_1 DEBUG    
03-25 00:07:26 pneuvironment_run_policy_run_final_1 DEBUG    : total      6510.5 (100.0%)
03-25 00:07:26 pneuvironment_run_policy_run_final_1 DEBUG    : get action 736.7 (11.3%)
03-25 00:07:26 pneuvironment_run_policy_run_final_1 DEBUG    : env step   267.2 (4.1%)
03-25 00:07:26 pneuvironment_run_policy_run_final_1 DEBUG    : train policy 70.7 (1.1%)
03-25 00:07:26 pneuvironment_run_policy_run_final_1 DEBUG    : other      5435.8 (83.5%)
03-25 00:07:26 pneuvironment_run_policy_run_final_1 DEBUG    
03-25 00:07:26 pneuvironment_run_policy_run_final_1 INFO     Iteration 3
03-25 00:07:26 pneuvironment_run_policy_run_final_1 INFO     Training policy...
03-25 00:09:10 pneuvironment_run_policy_run_final_1 INFO     Gathering rollouts...
03-25 01:13:47 pneuvironment_run_policy_run_final_1 INFO     Appending dataset...
03-25 01:13:47 pneuvironment_run_policy_run_final_1 INFO     -----------------  ----------------
03-25 01:13:47 pneuvironment_run_policy_run_final_1 INFO     Itr                     3
03-25 01:13:47 pneuvironment_run_policy_run_final_1 INFO     ReturnAvg               2.88258
03-25 01:13:47 pneuvironment_run_policy_run_final_1 INFO     ReturnMax              10.265
03-25 01:13:47 pneuvironment_run_policy_run_final_1 INFO     ReturnMin              -2.60954
03-25 01:13:47 pneuvironment_run_policy_run_final_1 INFO     ReturnStd               4.01758
03-25 01:13:47 pneuvironment_run_policy_run_final_1 INFO     TrainingLossFinal  570189
03-25 01:13:47 pneuvironment_run_policy_run_final_1 INFO     TrainingLossStart       4.89729e+07
03-25 01:13:47 pneuvironment_run_policy_run_final_1 INFO     -----------------  ----------------
03-25 01:13:47 pneuvironment_run_policy_run_final_1 DEBUG    
03-25 01:13:47 pneuvironment_run_policy_run_final_1 DEBUG    : total      3981.4 (100.0%)
03-25 01:13:47 pneuvironment_run_policy_run_final_1 DEBUG    : get action 738.7 (18.6%)
03-25 01:13:47 pneuvironment_run_policy_run_final_1 DEBUG    : env step   265.1 (6.7%)
03-25 01:13:47 pneuvironment_run_policy_run_final_1 DEBUG    : train policy 103.8 (2.6%)
03-25 01:13:47 pneuvironment_run_policy_run_final_1 DEBUG    : other      2873.7 (72.2%)
03-25 01:13:47 pneuvironment_run_policy_run_final_1 DEBUG    
03-25 01:13:47 pneuvironment_run_policy_run_final_1 INFO     Iteration 4
03-25 01:13:47 pneuvironment_run_policy_run_final_1 INFO     Training policy...
03-25 01:16:02 pneuvironment_run_policy_run_final_1 INFO     Gathering rollouts...
03-25 01:36:26 pneuvironment_run_policy_run_final_1 INFO     Appending dataset...
03-25 01:36:26 pneuvironment_run_policy_run_final_1 INFO     -----------------  -------------
03-25 01:36:26 pneuvironment_run_policy_run_final_1 INFO     Itr                     4
03-25 01:36:26 pneuvironment_run_policy_run_final_1 INFO     ReturnAvg               8.6461
03-25 01:36:26 pneuvironment_run_policy_run_final_1 INFO     ReturnMax              15.689
03-25 01:36:26 pneuvironment_run_policy_run_final_1 INFO     ReturnMin               0.904735
03-25 01:36:26 pneuvironment_run_policy_run_final_1 INFO     ReturnStd               4.48062
03-25 01:36:26 pneuvironment_run_policy_run_final_1 INFO     TrainingLossFinal  676090
03-25 01:36:26 pneuvironment_run_policy_run_final_1 INFO     TrainingLossStart  845262
03-25 01:36:26 pneuvironment_run_policy_run_final_1 INFO     -----------------  -------------
03-25 01:36:26 pneuvironment_run_policy_run_final_1 DEBUG    
03-25 01:36:26 pneuvironment_run_policy_run_final_1 DEBUG    : total      1359.1 (100.0%)
03-25 01:36:26 pneuvironment_run_policy_run_final_1 DEBUG    : get action 741.5 (54.6%)
03-25 01:36:26 pneuvironment_run_policy_run_final_1 DEBUG    : env step   262.5 (19.3%)
03-25 01:36:26 pneuvironment_run_policy_run_final_1 DEBUG    : train policy 135.3 (10.0%)
03-25 01:36:26 pneuvironment_run_policy_run_final_1 DEBUG    : other      219.9 (16.2%)
03-25 01:36:26 pneuvironment_run_policy_run_final_1 DEBUG    
03-25 01:36:26 pneuvironment_run_policy_run_final_1 INFO     Iteration 5
03-25 01:36:26 pneuvironment_run_policy_run_final_1 INFO     Training policy...
03-25 01:39:12 pneuvironment_run_policy_run_final_1 INFO     Gathering rollouts...
