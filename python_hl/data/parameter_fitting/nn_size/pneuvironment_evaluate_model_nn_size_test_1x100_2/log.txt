04-02 00:45:04 pneuvironment_evaluate_model_nn_size_test_1x100_2 INFO     Read random dataset from file
04-02 00:45:04 pneuvironment_evaluate_model_nn_size_test_1x100_2 INFO     Read init dataset from file
04-02 00:45:04 pneuvironment_evaluate_model_nn_size_test_1x100_2 INFO     Creating policy
04-02 00:45:05 pneuvironment_evaluate_model_nn_size_test_1x100_2 INFO     Training policy....
04-02 00:45:29 pneuvironment_evaluate_model_nn_size_test_1x100_2 INFO     Evaluating predictions...
04-02 00:45:39 pneuvironment_evaluate_model_nn_size_test_1x100_2 INFO     -----------------  -------------
04-02 00:45:39 pneuvironment_evaluate_model_nn_size_test_1x100_2 INFO     Eval loss               0.801641
04-02 00:45:39 pneuvironment_evaluate_model_nn_size_test_1x100_2 INFO     ReturnAvg              14.9583
04-02 00:45:39 pneuvironment_evaluate_model_nn_size_test_1x100_2 INFO     ReturnMax              16.4085
04-02 00:45:39 pneuvironment_evaluate_model_nn_size_test_1x100_2 INFO     ReturnMin              12.7591
04-02 00:45:39 pneuvironment_evaluate_model_nn_size_test_1x100_2 INFO     ReturnStd               1.58122
04-02 00:45:39 pneuvironment_evaluate_model_nn_size_test_1x100_2 INFO     TrainingLossFinal  279345
04-02 00:45:39 pneuvironment_evaluate_model_nn_size_test_1x100_2 INFO     TrainingLossStart  952478
04-02 00:45:39 pneuvironment_evaluate_model_nn_size_test_1x100_2 INFO     -----------------  -------------
04-02 00:45:39 pneuvironment_evaluate_model_nn_size_test_1x100_2 DEBUG    
04-02 00:45:39 pneuvironment_evaluate_model_nn_size_test_1x100_2 DEBUG    : total      33.5 (100.0%)
04-02 00:45:39 pneuvironment_evaluate_model_nn_size_test_1x100_2 DEBUG    : train policy 24.1 (71.8%)
04-02 00:45:39 pneuvironment_evaluate_model_nn_size_test_1x100_2 DEBUG    : other      9.4 (28.2%)
04-02 00:45:39 pneuvironment_evaluate_model_nn_size_test_1x100_2 DEBUG    
04-02 00:45:39 pneuvironment_evaluate_model_nn_size_test_1x100_2 INFO     All plots saved to folder
