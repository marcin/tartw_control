04-02 00:21:23 pneuvironment_evaluate_model_nn_size_test_2x300 INFO     Read random dataset from file
04-02 00:21:23 pneuvironment_evaluate_model_nn_size_test_2x300 INFO     Read init dataset from file
04-02 00:21:23 pneuvironment_evaluate_model_nn_size_test_2x300 INFO     Creating policy
04-02 00:21:25 pneuvironment_evaluate_model_nn_size_test_2x300 INFO     Training policy....
04-02 00:22:30 pneuvironment_evaluate_model_nn_size_test_2x300 INFO     Evaluating predictions...
04-02 00:22:41 pneuvironment_evaluate_model_nn_size_test_2x300 INFO     -----------------  ------------
04-02 00:22:41 pneuvironment_evaluate_model_nn_size_test_2x300 INFO     Eval loss               1.33867
04-02 00:22:41 pneuvironment_evaluate_model_nn_size_test_2x300 INFO     ReturnAvg              14.9583
04-02 00:22:41 pneuvironment_evaluate_model_nn_size_test_2x300 INFO     ReturnMax              16.4085
04-02 00:22:41 pneuvironment_evaluate_model_nn_size_test_2x300 INFO     ReturnMin              12.7591
04-02 00:22:41 pneuvironment_evaluate_model_nn_size_test_2x300 INFO     ReturnStd               1.58122
04-02 00:22:41 pneuvironment_evaluate_model_nn_size_test_2x300 INFO     TrainingLossFinal   78814.3
04-02 00:22:41 pneuvironment_evaluate_model_nn_size_test_2x300 INFO     TrainingLossStart  964036
04-02 00:22:41 pneuvironment_evaluate_model_nn_size_test_2x300 INFO     -----------------  ------------
04-02 00:22:41 pneuvironment_evaluate_model_nn_size_test_2x300 DEBUG    
04-02 00:22:41 pneuvironment_evaluate_model_nn_size_test_2x300 DEBUG    : total      75.3 (100.0%)
04-02 00:22:41 pneuvironment_evaluate_model_nn_size_test_2x300 DEBUG    : train policy 65.1 (86.4%)
04-02 00:22:41 pneuvironment_evaluate_model_nn_size_test_2x300 DEBUG    : other      10.3 (13.6%)
04-02 00:22:41 pneuvironment_evaluate_model_nn_size_test_2x300 DEBUG    
04-02 00:22:41 pneuvironment_evaluate_model_nn_size_test_2x300 INFO     All plots saved to folder
