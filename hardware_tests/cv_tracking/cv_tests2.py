# import the necessary packages
from collections import deque
from imutils.video import VideoStream
import numpy as np
import argparse
import cv2
import imutils
import time
 
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video",
        help="path to the (optional) video file")
ap.add_argument("-b", "--buffer", type=int, default=64,
        help="max buffer size")
args = vars(ap.parse_args())

# define the lower and upper boundaries of the "green"
# ball in the HSV color space, then initialize the
# list of tracked points
color_lower = (0, 109, 186)
color_upper = (37, 255, 255)
pts = deque(maxlen=args["buffer"])
 
# if a video path was not supplied, grab the reference
# to the webcam
if not args.get("video", False):
        vs = VideoStream(src=2).start()


 
# otherwise, grab a reference to the video file
else:
        vs = cv2.VideoCapture(args["video"])
 
# allow the camera or video file to warm up
time.sleep(2.0)

frame_count = 0
# keep looping
while True:
        starttime = time.time()
        # grab the current frame
        frame = vs.read()
 
        # handle the frame from VideoCapture or VideoStream
        frame = frame[1] if args.get("video", False) else frame
 
        # if we are viewing a video and we did not grab a frame,
        # then we have reached the end of the video
        if frame is None:
                break
 
        # resize the frame, blur it, and convert it to the HSV
        # color space
        frame = imutils.resize(frame, width=600)
        blurred = cv2.GaussianBlur(frame, (11, 11), 0)
        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
 
        # construct a mask for the color "green", then perform
        # a series of dilations and erosions to remove any small
        # blobs left in the mask
        mask = cv2.inRange(hsv, color_lower, color_upper)
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)


        # find contours in the mask and initialize the current
        # (x, y) center of the ball
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        center = None
 
        # only proceed if at least one contour was found
        if len(cnts) > 0:
                frame_count += 1
                # find the largest contour in the mask, then use
                # it to compute the minimum enclosing circle and
                # centroid
                c = max(cnts, key=cv2.contourArea)
                # (x, y, w, h) = cv2.boundingRect(c)
                ((x, y), radius) = cv2.minEnclosingCircle(c)
                M = cv2.moments(c)
                center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
                # print(center)
                # print(x, y, w, h)


                # only proceed if the radius meets a minimum size
                # if w > 10  and h > 10:
        if radius > 10.0:
                        # draw the circle and centroid on the frame,
                        # then update the list of tracked points
                        # cv2.rectangle(frame,(x,y), (x+w, y+h), (0, 255, 255), 2)
            cv2.circle(frame, (int(x), int(y)), int(radius),
                       (0, 255, 255), 2)
 
        # show the frame to our screen
        cv2.imshow("Frame", frame)
        key = cv2.waitKey(1) & 0xFF

        
        if frame_count > 10:
            print(10 / (time.time() - starttime))
            frame_count = 0


 
        # if the 'q' key is pressed, stop the loop
        if key == ord("q"):
                break
 
# if we are not using a video file, stop the camera video stream
if not args.get("video", False):
        vs.stop()
 
# otherwise, release the camera
else:
        vs.release()
 
# close all windows
cv2.destroyAllWindows()
