//* test the pressure sensor and library*/

# include "psense_honeywell.h"

# define SENSOR_CS_PIN 35
# define MAX_PRESSURE 2.068
# define MIN_PRESSURE 0.0
# define MAX_OUTPUT 0.9
# define MIN_OUTPUT 0.1

PSensor sensor = PSensor();
float pressure;


void setup() {
  sensor.begin(SENSOR_CS_PIN, MAX_PRESSURE, MIN_PRESSURE, MAX_OUTPUT, MIN_OUTPUT);
  Serial.begin(9600); 
  while (!Serial) ;
}

void loop() {
  static boolean status = false;
  pressure = sensor.ReadPressure();
  Serial.print("Pressure: ");
  Serial.println(pressure);

  delay(1000);
}
