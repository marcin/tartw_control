/*
Library for interfacing an Arduino with Honeywell ABP pressure sensors.
MM - 2018
*/

# ifndef HONEYWELL_ABP
# define HONEYWELL_ABP
#include <SPI.h>

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

class PSensor
{
  public:
    PSensor();
    ~PSensor();
    uint8_t begin(uint8_t pin_CS, float max_pressure, float min_pressure, float max_output, float min_output,  boolean read_temp = false);
    float ReadPressure(void);
  private:
    uint8_t _pin_CS;
    boolean _read_temp;
    float _prange, _pmin;
    uint16_t _outrange, _outmin;
    float TransferFunPress(uint16_t datawords);
    uint8_t GetStatus(uint16_t statusword);
};

# endif
