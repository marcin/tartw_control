/*
    Sketch to test valve driver board
*/

#define NO_VALVES 8
#include "pneumatic_channel.h"

// Defines


// Data arrays
PBoard board(basic_valve_pins, basic_sensor_pins, 
             MAX_PRESSURE, MIN_PRESSURE, MAX_OUTPUT, MIN_OUTPUT);
float pressures[NO_VALVES];
elapsedMillis pressure_print_time;



void setup() {
    Serial.begin(9600);
    while (!Serial) ;
}

void loop() {
    static int i;
    static int valve = 0;
    static int new_valve = 0;
    static char command;
    static bool pressure_print = false;
    static int pressure_level;
    static float pressure_level_float;

    if (Serial.available()) {
        command = Serial.read();
        switch(command) {
            case 'u':
                board.modulate(valve, board.channels[valve].duty_cycle + 64);
                Serial.print("Modulating pin ");
                Serial.println(board.channels[valve].valve_pin);
                break;
            case 'd':
                board.modulate(valve, board.channels[valve].duty_cycle - 64);
                break;
            case 't':
                board.toggle(valve);
                break;
            case 'a':
                pressure_print = !pressure_print;
                break;
            case 'p':
                while (!Serial.available()) ;
                pressure_level = Serial.read() - '0';
                Serial.print("Setting pressure level to: ");
                Serial.println(pressure_level);
                pressure_level_float = (float)pressure_level * (MAX_PRESSURE -
                                        MIN_PRESSURE) / 10.0;
                board.setPressure(valve, pressure_level_float); 
                break;
            case 'v':
                while (!Serial.available()) ;
                new_valve = Serial.read() - '0';  
                if (new_valve >= 0 && new_valve < NO_VALVES) {
                    valve = new_valve; 

                }
                else {
                    Serial.println("Incorrect valve number");
                }
                break;
            default:
                Serial.println("Incorrect command. u to increase pulse width,"
                                "d to decrease, t to toggle on/off, p to get "
                                "pressure");
                break;
        }
        for (i=0; i<NO_VALVES; i++) {
            Serial.print("valve ");
            Serial.print(i);
            Serial.print(": ");
            Serial.println(board.channels[i].duty_cycle);
        }
        Serial.print("Current valve: ");
        Serial.println(valve);
    }
    
    if (pressure_print && (pressure_print_time > 500)) {
    pressure_print_time = pressure_print_time - 500;
    board.readPressureAllFiltered(pressures);
    for (i = 0; i<NO_VALVES; i++) {
        Serial.print(i);
        Serial.print(": ");
        Serial.print(pressures[i]);
        Serial.print(" bar ");
    }
    Serial.println("");
    }
    board.run();
}
