/*
Library for interfacing an Arduino with Honeywell ABP and TruStability pressure sensors through SPI.
Conversion happens acording to https://sensing.honeywell.com/spi-comms-digital-ouptu-pressure-sensors-tn-008202-3-en-final-30may12.pdf
You can get sensor max_pressure, min_pressure etc. from Figure 4 in the datasheet:
https://sensing.honeywell.com/honeywell-sensing-basic-board-mount-pressure-abp-series-datasheet-2305128-e-en.pdf
mm - 2018
*/

# include "psense_honeywell.h"
# include <SPI.h>



// global variables for this sketch
static SPISettings ABP_spisettings = SPISettings(500000, MSBFIRST, SPI_MODE0);

/*
Constructor
Parameters:
----------------
  none
*/
PSensor::PSensor()
{

}

PSensor::~PSensor()
{

}
/*
Initialises the sensor object
Parameters:
----------------
  pin_CS:
    chip select pin for the chip

Returns:
----------------
  Status: uint8_t
    If there was an error during initialisation, return 1, otherwise 0
*/

uint8_t PSensor::begin(uint8_t pin_CS, float max_pressure, float min_pressure, float max_output, float min_output) {

  _pin_CS = pin_CS;
  _prange = max_pressure - min_pressure;
  // range of values is 2^NO_BITS
  _outrange = (max_output  - min_output) * (0x01 << NO_BITS); 
  _pmin = min_pressure;
  _outmin = min_output * (0x01 << NO_BITS);

  // Set CS pin to output
  pinMode(_pin_CS, OUTPUT);
  digitalWrite(_pin_CS, HIGH);

  // Start SPI
  SPI.begin();
  // Check if chip is OK
  delay(5); // Maximum chip startup time is 5ms, this is to let it settle down
  uint8_t status;
  if (ReadPressure() == -1.0) {
    status = -1;
  } else {
    status = 0;
  }
  return status;
}

/*
Reads pressure from the chip

Parameters:
-------------------------
  None
Returns:
------------------------
  pressure: float
    pressure value in Pa, if -1 then an error occured
*/
float PSensor::ReadPressure(void)
{
  // First, we read the sensor's response. It's in the first two are for pressure:
  // B1 is S[1:0]D[13:8], B2 is D[7:0]
  // The next two are for temperature (not implemented yet)
  // Where S are status bits ans D are data bits

  uint16_t spi_pressureword;
//  uint16_t spi_temperatureword;
  float pressure;
  //Serial.println(_pin_CS);
  // Start SPI transaction
  digitalWrite(_pin_CS, LOW);
  SPI.beginTransaction(ABP_spisettings);

  // Get pressure
  spi_pressureword = SPI.transfer16(0x0000);

  // Get temperature
  //if (_read_temp) {
    //spi_temperatureword = SPI.transfer16(0x0000);
  //}

  // End SPI
  SPI.endTransaction();
  digitalWrite(_pin_CS, HIGH);

  // check status word
  if (GetStatus(spi_pressureword)) {
    // If there's an error, return -1
    return -1;
  }
  // if everything's OK, return pressure
  //Serial.println(spi_pressureword);
  return pressure = TransferFunPress(spi_pressureword);
}

/*
Checks status read from the chip
Parameters:
-------------------------
  statusword:
    word for the status
Returns:
------------------------
   1 if there was an error, 0 otherwise
*/
uint8_t PSensor::GetStatus(uint16_t statusword) {
  // Two highest bits of the pressure reading are for status. Status 0x03
  // means there was an error. 0x01 would mean we're reading old data, but
  // this is not implemented here

  if ((statusword >> 14) == 0x03)
  {
    return 1;
  } else {
    return 0;
  }
}

/*
Reads pressure from the chip

Parameters:
-------------------------
  datawords:
    data words from the sensor
Returns:
------------------------
  pressure: float
    pressure value in Pa
*/
float PSensor::TransferFunPress(uint16_t datawords)
{
  return (float) (datawords - _outmin) * _prange / _outrange + _pmin;
}
