/*
Class for simple, reasonably robust communication through Arduino serial
*/

# ifndef EASYSERIAL
# define EASYSERIAL



#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

// Some constants for the trasmission

# define START_MARKER 0x7E
# define END_MARKER 0x7E
# define ESCAPE_MARKER 0x7D
# define TIMEOUT 500
# define MESSAGE_SIZE 128

// Commands
# define HELLO 0x00
# define ACTUATE_VALVE 0x01
# define READ_SENSOR 0x02
# define ACKNOWLEDGE 0xFF

// Defines
# define SERIAL_OBJ SerialUSB
# define DEBUGSERIAL_OBJ Serial

union float_serial {
  float val;
  uint8_t bytes[4];
};

class EasySerial{
  public:
    EasySerial();
    ~EasySerial();
    uint8_t handshake();
    uint8_t begin(int baudrate);
    uint8_t available();
    uint8_t *read();
    void write(uint8_t *data, int data_length);
    uint8_t buffer[MESSAGE_SIZE];
    uint8_t last_message[MESSAGE_SIZE];
    uint8_t last_message_length;
    boolean message_available;
  private:
    void _copy_msg(uint8_t *msg, uint8_t *buf, uint8_t msg_len);
    uint8_t _baudrate;
    uint8_t _max_length;
    boolean _transfer_complete;
    boolean initialised;
    enum read_states {
      IDLING,
      STARTED,
      READING,
      FOUND_SPECIAL,
    } READ_STATE;
};
# endif
