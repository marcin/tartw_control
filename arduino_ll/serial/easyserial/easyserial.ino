/*
  Sketch to test the serial protocol
*/

// Start and end markers for the transmission
# include "easyserial.h"
uint8_t testmsg[160] =  {0x01, 0x02};

EasySerial testserial;
int message_len;
uint8_t message_buf;
int starttime;
void setup() {
  DEBUGSERIAL_OBJ.begin(9600);
  testserial.begin(230400);
  pinMode(LED_BUILTIN, OUTPUT);
  DEBUGSERIAL_OBJ.println("Starting");
  digitalWrite(LED_BUILTIN, HIGH);
}

void loop() {
  if (message_len = testserial.available()) {
    starttime = micros();
    testserial.write(testserial.read(), message_len);
    DEBUGSERIAL_OBJ.print(micros() - starttime);
  }
}
