# include "easyserial.h"


#ifdef DEBUGSERIAL
  #define debug(x) x
#else
  #define debug(x) do { } while(0)
#endif

EasySerial::EasySerial()
{

}

EasySerial::~EasySerial()
{

}

/*
Initialise serial communication

Parameters:
----------------------
baudrate: int
Baudrate of the communication (try to use the default 9600, 57600, 111520 etc. series)

Returns:
----------------------
-1 if an error occured, 0 if everything went OK
*/

uint8_t EasySerial::begin(int baudrate)
{
  // Start serial communication
  SERIAL_OBJ.begin(baudrate);
  // Try to do a handshake, if it fails, return -1
  if (handshake()) {
    return -1;
  }
  return 0;
}

/*
Perform a handshake with the other computer

Parameters:
----------------------
None

Returns:
----------------------
-1 if an error occured, 0 if everything went OK
*/

uint8_t EasySerial::handshake()
{
  return 0;
}

/*
Combine data into a packet and print it out through serial
Parameters:
----------------------
command: uint8_t
Command from the command list
data: array of uint8_t
data to be sent

Returns:
----------------------
-1 if an error occured, 0 if everything went OK
*/

void EasySerial::write(uint8_t *data, int data_length)
{
  int no_written;
  int i;
  // This is not very elegant, but the transfer is asynchronous so it's probably
  // faster to do it that way than to concatenate the arrays
  SERIAL_OBJ.write(START_MARKER);
  for(i = 0; i < data_length; i++) {
    if (*data != START_MARKER && *data != ESCAPE_MARKER && *data != END_MARKER) {
      SERIAL_OBJ.write(*data++);
    } else {
      SERIAL_OBJ.write(ESCAPE_MARKER);
      SERIAL_OBJ.write(*data++);
    }
  }
  SERIAL_OBJ.write(END_MARKER);
}

/*
Read data from the serial port, if available

Parameters:
----------------------------------------

Returns:
----------------------------------------
packet: bytes
Packet to be sent over serial
*/
uint8_t EasySerial::available()
{
  //DEBUGSERIAL_OBJ.println("Entering the available() fun");
  static uint8_t *buffer_p = buffer;
  static uint8_t current_position = 0;
  uint8_t no_in_buffer;
  uint8_t ch;
  int i;


  switch (READ_STATE)
  {
    case IDLING:
    {
      // If we were not doing anything up until now, check if there's anything
      // in the serial buffer
      if (SERIAL_OBJ.available()){
        // If there was, start reading it
        READ_STATE = STARTED;
      }
      else
      // If there wasn't, stop
      break;
    }

    case STARTED:
    {
      // Read stuff from the buffer looking for a start marker
      if (no_in_buffer = SERIAL_OBJ.available()) { // get number of stuff in the buffer
        for (i = 0; i < no_in_buffer; i++) {
          // read buffer until we find the start marker
          if (SERIAL_OBJ.read() == START_MARKER) {
            READ_STATE = READING;
            break;
          }
        }
      }
      break;
    }

    case READING:
    {
      // Read stuff from serial into the buffer
      // If there's stuff in the buffer
      if (no_in_buffer = SERIAL_OBJ.available()) {
        // Read all the bytes, store them in the buffer
        for (i = 0; (i < no_in_buffer); i++) {
          // Get byte from serial buffer
          ch = SERIAL_OBJ.read();

          // Check if there's a special marker
          if (ch == ESCAPE_MARKER) {
            READ_STATE = FOUND_SPECIAL;
            break;
          }
          // If not special, check if we've got the terminating byte
          else if (ch == END_MARKER) {
            // If we did, copy the message to the last one
            _copy_msg(buffer, last_message, current_position);
            last_message_length  = current_position;
            message_available = true;
            buffer_p = buffer;
            READ_STATE = IDLING;
            current_position = 0;
            break;
          }
          // If not any of those, it's a normal char
          else {
            *buffer_p++ = ch;
            current_position++;
          }
        }
      }
      break;
    }

    case FOUND_SPECIAL:
    {
      // If we found the special marker, we need to save the
      // next char directly into buffer
      if (SERIAL_OBJ.available()) {
        ch = SERIAL_OBJ.read(); // pop next char from serial buffer
        *buffer_p++ = ch; // save it
        current_position++; // count one char up
        READ_STATE = READING;
      }
      break;
    }
  }

  if (message_available) {
    return last_message_length;
  } else {
    return 0;
  }
}



/*
Simple helper function to mimic the functionality of SERIAL_OBJ.read()
Parameters:
----------------------------------------

Returns:
----------------------------------------
p: *uint8_t
pointer to last_message[0]
*/

uint8_t *EasySerial::read()
{
  uint8_t *p = last_message;
  message_available = false;
  return p;
}

/*
Copy message into buffer
*/
void EasySerial::_copy_msg(uint8_t *msg, uint8_t *buf, uint8_t msg_len)
{
  int i;
  for (i = 0; i <= msg_len; i++) {
    *buf++ = *msg++;
  }
}
