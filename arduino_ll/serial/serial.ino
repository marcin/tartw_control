/* -----------------------------------------------------------------------------
 * Example .ino file for arduino, compiled with CmdMessenger.h and
 * CmdMessenger.cpp in the sketch directory.
 *----------------------------------------------------------------------------*/

#include "CmdMessenger.h"

/* Initialize CmdMessenger -- this should match PyCmdMessenger instance */
# define NO_SENSORS 1
# define NO_VALVES 1
const int BAUD_RATE = 9600;
const int VALVE_PINS[NO_VALVES] {25};


/* Define available CmdMessenger commands */
enum {
  valve_switch,
};


CmdMessenger cmd_messenger = CmdMessenger(Serial,',',';','/');

/* Create callback functions to deal with incoming messages */

/* callbacks */

void on_valve_switch(void){
    int i;
    boolean state;
    for (i = 0; i < NO_VALVES; i++) {
      state = cmd_messenger.readBinArg<bool>();
      digitalWrite(VALVE_PINS[i], state ? HIGH : LOW);
    }
}

/* Attach callbacks for CmdMessenger commands */
void attach_callbacks(void) {
    cmd_messenger.attach(valve_switch, on_valve_switch);
}

void setup() {
    int i;
    // Set valve pins to output
    for (i = 0; i < NO_VALVES; i++) {
      pinMode(VALVE_PINS[i], OUTPUT);
    }
    // Start serial comms
    Serial.begin(BAUD_RATE);
    attach_callbacks();
}

void loop() {
    cmd_messenger.feedinSerialData();
}
