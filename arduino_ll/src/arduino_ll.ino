/*
 *  Sketch for low-level control of a pneumatic control board
 *  for the TARTW project. The Arduino communicates with the computer using the
 *  CmdMessenger protocol. It reads pressures from Honeywell pressure sensors
 *  using the psense_honeywell library, and accelerometer data from the BNO055
 *  chip using the Adafruit library. 
 */

/*
TODO:
    1. Add accelerometer calibration checks
*/

// Define hardware parameters
#define NO_VALVES 8
#define BAUD_RATE 230400
#define SERIAL_MAIN Serial
//#define DEBUG
// Libraries
 #include "psense_honeywell.h"
 #include "CmdMessenger.h"
 #include "pneumatic_channel.h"
 #include "Adafruit_BNO055.h" 
 #include "Adafruit_Sensor.h"
 #include "imumaths.h"
 #include "Wire.h"
 #include "tartw_utils.h"



/*
--------------Serial comm setup prototypes--------------------
*/

// Create new messenger instance
CmdMessenger cmd_messenger = CmdMessenger(Serial,',',';','/');

/* Define available CmdMessenger commands */
enum {
  Kerror,
  Kvalve_switch_multiple,
  Kvalve_switch_single,
  Kvalve_modulate_single,
  Kvalve_set_pressure,
  Krequest_pressure_readings,
  Ksend_pressure_readings,
  Krequest_accelerometer_readings,
  Ksend_accelerometer_readings,
  Krequest_orientation,
  Ksend_orientation,
  Krequest_all_sensor_data,
  Ksend_all_sensor_data,
  Krequest_IMU_calibration,
  Ksend_IMU_calibration,
};


// messenger function prototypes - they manage how the uC will respond to a
// command
void attach_callbacks(void);
void on_unknown_command(void);
void on_valve_switch_multiple(void);
void on_valve_switch_single(void);
void on_valve_modulate_single(void); 
void on_valve_set_pressure(void);
void on_request_pressure_readings(void);
void on_request_accelerometer_readings(void);
void on_request_orientation(void);
void on_request_all_sensor_data(void);
void on_request_IMU_calibration(void);

/*
---------- Other prototypes -----------------------------
*/

// Initialise board
PBoard board(basic_valve_pins, basic_sensor_pins, 
             MAX_PRESSURE, MIN_PRESSURE, MAX_OUTPUT, MIN_OUTPUT);

Adafruit_BNO055 bno055_imu = Adafruit_BNO055();
void initialise_bno055(void);

void setup() {
   // Initialise main serial
   SERIAL_MAIN.begin(BAUD_RATE);
   while (!SERIAL_MAIN) ;
   // Initialise SERIAL_DEBUG comms
   SERIAL_DEBUG.begin(230400);
   while (!SERIAL_DEBUG) ;
   // Attach CmdMessenger Callbacks
   attach_callbacks();
   // Initialise sensors
   initialise_bno055();
}


void loop() {
   cmd_messenger.feedinSerialData();
   board.run();
 }


/*
 * ------------------ Messenger functions ----------------------------
 */
 


/* Attach callbacks for CmdMessenger commands */
void attach_callbacks(void) {
    cmd_messenger.attach(Kerror, on_unknown_command);
    cmd_messenger.attach(Kvalve_switch_multiple, on_valve_switch_multiple);
    cmd_messenger.attach(Kvalve_switch_single, on_valve_switch_single);
    cmd_messenger.attach(Kvalve_modulate_single, on_valve_modulate_single);
    cmd_messenger.attach(Kvalve_set_pressure, on_valve_set_pressure);
    cmd_messenger.attach(Krequest_pressure_readings, on_request_pressure_readings);
    cmd_messenger.attach(Krequest_accelerometer_readings,
                         on_request_accelerometer_readings);
    cmd_messenger.attach(Krequest_orientation, on_request_orientation);
    cmd_messenger.attach(Krequest_all_sensor_data, on_request_all_sensor_data);
    cmd_messenger.attach(Krequest_IMU_calibration, on_request_IMU_calibration);
}


/* Create callback functions to deal with incoming messages */

/* callbacks */

void on_unknown_command(void)
{
  cmd_messenger.sendCmd(0,"Unknown command");
}

void on_valve_switch_multiple(void){
    /*
    Switches valves on and off as requested by the master
    */
    int i;
    boolean state;
    for (i = 0; i < NO_VALVES; i++) {
      state = cmd_messenger.readBinArg<bool>();
      board.actuate(i, state);
    }
}

void on_valve_switch_single(void) {
    /*
    Switches a single valve on or off
    */
    int valve = cmd_messenger.readBinArg<int>();
    boolean state = cmd_messenger.readBinArg<bool>();

    board.actuate(valve, state);
}

void on_valve_modulate_single(void) {
    /*
    Modulates a single valves with give duty cycle
    */
    int valve = cmd_messenger.readBinArg<int>();
    int duty_cycle = cmd_messenger.readBinArg<int>();
    board.modulate(valve, duty_cycle);
}

void on_valve_set_pressure(void) {
    /*
    Sets pressure in a valve
    */
    int valve = cmd_messenger.readBinArg<int>();
    float pressure = cmd_messenger.readBinArg<float>();
    board.setPressure(valve, pressure);
}

void on_request_pressure_readings(void){
    /*
    Reads pressure and reports results back to master
    */
    
    float pressures[NO_VALVES];
    int i;

    board.readPressureAllFiltered(pressures); 


    cmd_messenger.sendCmdStart(Ksend_pressure_readings);
    for (i = 0; i < NO_VALVES; i++) {
      cmd_messenger.sendCmdBinArg<float>(pressures[i]);
    }
    cmd_messenger.sendCmdEnd();
}

void on_request_accelerometer_readings(void){
    /*
    Reads accelerations and reports results back to master
    */
    
    imu::Vector<3> acceleration_data =
                    bno055_imu.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);

    cmd_messenger.sendCmdStart(Ksend_accelerometer_readings);

    cmd_messenger.sendCmdBinArg<float>(acceleration_data.x());
    cmd_messenger.sendCmdBinArg<float>(acceleration_data.y());
    cmd_messenger.sendCmdBinArg<float>(acceleration_data.z());

    cmd_messenger.sendCmdEnd();
}


void on_request_orientation(void){
    /*
    Reads orientations and reports results back to master
    */
    
    imu::Quaternion orientation_data = bno055_imu.getQuat(); 

    cmd_messenger.sendCmdStart(Ksend_orientation);

    cmd_messenger.sendCmdBinArg<float>(orientation_data.w());
    cmd_messenger.sendCmdBinArg<float>(orientation_data.x());
    cmd_messenger.sendCmdBinArg<float>(orientation_data.y());
    cmd_messenger.sendCmdBinArg<float>(orientation_data.z());

    cmd_messenger.sendCmdEnd();
}

void on_request_all_sensor_data(void){
    /*
    Reads all_sensors and reports results back to master
    */
    
    int i;
    float pressures[NO_VALVES];

    imu::Quaternion orientation_data = bno055_imu.getQuat();
    board.readPressureAll(pressures); 


    cmd_messenger.sendCmdStart(Ksend_all_sensor_data);

    for (i = 0; i < NO_VALVES; i++) {
      cmd_messenger.sendCmdBinArg<float>(pressures[i]);
    }

    cmd_messenger.sendCmdBinArg<float>(orientation_data.w());
    cmd_messenger.sendCmdBinArg<float>(orientation_data.x());
    cmd_messenger.sendCmdBinArg<float>(orientation_data.y());
    cmd_messenger.sendCmdBinArg<float>(orientation_data.z());

    cmd_messenger.sendCmdEnd();
}

void on_request_IMU_calibration(void)
{
    /*
    Sends calibration data
    */
    uint8_t system_stat, gyro_stat, accel_stat, mag_stat = 0;

    bno055_imu.getCalibration(&system_stat, &gyro_stat, &accel_stat, &mag_stat);

    cmd_messenger.sendCmdStart(Ksend_IMU_calibration);
    cmd_messenger.sendCmdBinArg<int>(system_stat);
    cmd_messenger.sendCmdBinArg<int>(gyro_stat);
    cmd_messenger.sendCmdBinArg<int>(accel_stat);
    cmd_messenger.sendCmdBinArg<int>(mag_stat);
    cmd_messenger.sendCmdEnd();

}


/* 
 * -------------------------- Other functions --------------------------
 */


void initialise_bno055(void) {
    /*
    Starts the IMU. For some reason it doesn't seem to be getting the operation
    mode command sometimes. I'll try sending it twice.
    */
    uint8_t system_stat, gyro_stat, accel_stat, mag_stat = 0;

    if (!bno055_imu.begin())
    {
        cmd_messenger.sendCmd(0, "Could not connect to IMU");
        DEBUG_PRINT("Imu connection error");
        while(1);
    }

    delay(1000);

    bno055_imu.setExtCrystalUse(true);
}

