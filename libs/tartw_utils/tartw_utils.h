/* Utility functions for the tartw project
 *
 */
 
// Debug macros
#define SERIAL_DEBUG Serial1

// Debugger stuff
#ifdef DEBUG
# define DEBUG_PRINT(x) Serial1.println(x) 
#else
# define DEBUG_PRINT(x) do {} while (0)
#endif
