/*
 * Functions for moving valves and reading pressures
 *
 * mm, February 2019
 *
 */

#ifndef PRESSURE_CHANNEL_H_
#define PRESSURE_CHANNEL_H_

#include "psense_honeywell.h"
#include "PID_v1.h"
#include "tartw_utils.h"

#if (ARDUINO >= 100)
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

/* ------------ Defining all hardware parameters --------------- */

#ifndef NO_VALVES
    #define NO_VALVES 8
#endif

#ifndef NO_BOARDS
	#define NO_BOARDS 1
#endif

//----------------- Microcontroller parameters----------------

extern int basic_valve_pins[NO_VALVES]; 
extern int basic_sensor_pins[NO_VALVES];

// ---------------- PWM setup -------------------------------
#define PWM_RESOLUTION 12 // resolution of the analogWrite function in bits
#define PWM_FREQUENCY 20// PWM frequency
/* 
NOTE: Changing PWM frequency on other Arduinos than the Teeensy may break other
timer-dependent functions (millis() etc.). The PWM frequency modifying code also 
depends on the particular microcontroller, and was not implemented here. 
*/
const int max_pwm_absolute = 0x01 << PWM_RESOLUTION; // maximum PWM setpoint possible
const int mid_pwm_absolute = max_pwm_absolute / 2; 

// maximum PWM dc which makes sense for the valve. it will simply stay open if driven higher than this
#define MAX_PWM_VALVE 2800.0
// minimum PWM dc which makes sense for the valve. it will simply stay closed if driven lower than this
#define MIN_PWM_VALVE 600.0

// ------------------Pressure sensor parameters----------------

#define MAX_PRESSURE 2.068 // maximum pressure that the sensor can read 
#define MIN_PRESSURE 0.0 // minimum pressure that the sensor can read
// The next two constants are related to how the sensor converts pressure to
// integer values. Have a look at the psense_honeywell library and Honeywell
// Technical Note no. 008202
#define MAX_OUTPUT 0.9 
#define MIN_OUTPUT 0.1

// Pressure filtering parameters
#define FILTER_TIMER_PERIOD 2000 // the pressures will be filtered every 2000 us
#define FILTER_ALPHA 0.01
#define COMPLEMENT_ALPHA 0.99
//

// ---------------Pressure controller parameters---------------
#define PRESSURE_CONTROLLER_TIMER_PERIOD 2000 // controller will adjust pressure every 2000 us
#define PRESSURE_CONTROLLER_GAIN_P 10000.0
#define PRESSURE_CONTROLLER_GAIN_I 2000.0





/* ----------- Data structures ---------------------------------- */

typedef struct
{
    int valve_pin;
    int duty_cycle;
    float pressure_setpoint;
    boolean automatic_pressure;
    PSensor sensor;
} PChannel_t;

class PBoard
{
	
    public:
        PBoard(int valve_pins[], int sensor_pins[], 
               float max_pressure, float min_pressure,
               float max_output, float min_output);
        ~PBoard();
        float readPressureSingle(int valve_no);
        int readPressureAll(float pressures[]);
        float readPressureSingleFiltered(int valve_no);
        void readPressureAllFiltered(float pressures[]);
        void actuate(int valve_no, bool state);
        void modulate(int valve_no, int dc);
        void toggle(int valve_no);
        void setPressure(int valve_no, float pressure);
        void disablePressureControlAll();
        void disablePressureControlSingle(int channel_no);
        PChannel_t channels[NO_VALVES];
        void run(void);
    private:
        void filterStep();
        void pressureControlStep();
		float pressures[2][NO_VALVES] = {{0}};
        IntervalTimer pressureTimer;
        elapsedMicros pressureFilterTime;
        elapsedMicros pressureControllerTime;
};


#endif

