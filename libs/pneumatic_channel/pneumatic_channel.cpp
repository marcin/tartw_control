#include "pneumatic_channel.h"



int basic_valve_pins[NO_VALVES] = {2, 3, 4, 5, 6, 7, 8, 9};
int basic_sensor_pins[NO_VALVES] = {33, 34, 35, 36, 37, 38, 39, 14};
//int basic_valve_pins[NO_VALVES] = {6, 7, 8, 9};
//int basic_sensor_pins[NO_VALVES] = {37, 38, 39, 14};
/*
 * Constructor
 * Parameters:
 * ---------------
 *  none
 * Returns:
 * --------------
 *  none
 */
//PBoard::PBoard()
//{

//}

/*
 * Destructor
 * Parameters:
 * ---------------
 *  none
 * Returns:
 * --------------
 *  none
 */
PBoard::~PBoard()
{

}


/*
 * Initialises the channel.
 * Parameters:
 * ---------------
 *  valve_pins - pins which drive the solenoid valves
 *  sensor_pins - pins connected to CS* of the SPI pressure sensors
 *  max_pressure, min_pressure, max_output, min_output - variables needed to
 *  read data from the pressure sensor. Check the psense_honeywell header for
 *  details
 * 
 * Returns:
 * --------------
 *  0 if successful, -1 if error
 */
PBoard::PBoard(int valve_pins[NO_VALVES], int sensor_pins[NO_VALVES], 
               float max_pressure, float min_pressure, 
               float max_output, float min_output)
{
    int i;


    for (i = 0; i < NO_VALVES; i++) {
        // Assign values
        channels[i].valve_pin = valve_pins[i];

        // Set up valve pin
        pinMode(valve_pins[i], OUTPUT);
        analogWriteFrequency(valve_pins[i], PWM_FREQUENCY);
        analogWriteResolution(PWM_RESOLUTION);
        // Initialise the pressure sensor
        channels[i].sensor.begin(sensor_pins[i], max_pressure, min_pressure, 
                                 max_output, min_output);
        // Initialise valve & pressure control PID
        actuate(i, false); // Make sure valve is closed
        channels[i].duty_cycle = 0;
        channels[i].pressure_setpoint = 0;
        channels[i].automatic_pressure = false;
   }
//    pressureTimer.begin(filterStep, FILTER_TIMER_PERIOD); 
}
 
    
/*
 * Toggles the state of a valve
 * Parameters:
 * ---------------
 *  none
 * Returns:
 * --------------
 *  none
 */
void PBoard::toggle(int channel_no)
{
    if (channels[channel_no].automatic_pressure) {
        disablePressureControlSingle(channel_no);
        }

    boolean state = channels[channel_no].duty_cycle > mid_pwm_absolute;
    /* Toggles the valve state */
    if (state) {
        channels[channel_no].duty_cycle = 0;
    } else {
        channels[channel_no].duty_cycle = max_pwm_absolute;
    }
    analogWrite(channels[channel_no].valve_pin, (state == true ? 0 : max_pwm_absolute));
}


/*
 * Sets the duty cycle of the PWM wave driving a solenoid
 * Parameters:
 * ---------------
 *  channel_no - number of the channel for which the change is to be made
 *  dc - desired duty cycle
 * Returns:
 * --------------
 *  none
 */
void PBoard::modulate(int channel_no, int dc) 
{
    analogWrite(channels[channel_no].valve_pin, dc);
    channels[channel_no].duty_cycle = dc;
}


/*
 * Turns valve on/off
 * Parameters:
 * ---------------
 *  channel_no - number of the channel
 *  state - True is valve is to be switched on, False if it's to be switched
 *  off
 * Returns:
 * --------------
 *  none
 */
void PBoard::actuate(int channel_no, boolean state)
{
    if (channels[channel_no].automatic_pressure) {
        disablePressureControlSingle(channel_no);
        }
    analogWrite(channels[channel_no].valve_pin, state == true ? max_pwm_absolute : 0);
    channels[channel_no].duty_cycle = (state == true ? max_pwm_absolute :  0);
}



/*
 * Reads pressure from a pressure sensor on one of the channels
 * Parameters:
 * ---------------
 *   channel_no - number of the channel
 * Returns:
 * --------------
 *  pressure if successful, -1.0 if error
 */
float PBoard::readPressureSingle(int channel_no)
{
    return channels[channel_no].sensor.ReadPressure();
}


/*
 * Reads pressure from all the channels
 * Parameters:
 * ---------------
 *  pressures - array where pressures are supposed to be saved 
 * Returns:
 * --------------
 *  0 if successful, -1 if error
 */
int PBoard::readPressureAll(float pressures[])
{
    int i;

    for (i = 0; i < NO_VALVES; i++) {
        if ((pressures[i] = channels[i].sensor.ReadPressure()) == -1.0) {
            return -1;
        }
        DEBUG_PRINT(pressures[i]);
    }
    return 0;


}

/*
 *           PRESSURE CONTROL STUFF
 *
 */



/*
 * Sets the pressure setpoint for valve pressure controller
 * Parameters:
 * ---------------
 *  channel_no - number of the channel
 *  pressure- desired pressure
 * Returns:
 * --------------
 *  none
 */
void PBoard::setPressure(int channel_no, float pressure)
{
    if (!channels[channel_no].automatic_pressure) {
        channels[channel_no].automatic_pressure = true;
    }
    channels[channel_no].pressure_setpoint = pressure;
}


/*
 * Disables automatic pressure control for all valves
 * Parameters:
 * ---------------
 *  none
 * Returns:
 * --------------
 *  none
 */
void PBoard::disablePressureControlAll()
{
    int i;

    for (i = 0; i < NO_VALVES; i++) {
        channels[i].automatic_pressure = false;
        channels[i].pressure_setpoint = 0;
    }
}


/*
 * Disables automatic pressure control for one  valve
 * Parameters:
 * ---------------
 *  channel_no - number of channel for which control is to be disabled
 * Returns:
 * --------------
 *  none
 */
void PBoard::disablePressureControlSingle(int channel_no)
{
        channels[channel_no].automatic_pressure = false;
        channels[channel_no].pressure_setpoint = 0;
}

/*
 * Runs filtering function (ISR). I know it repeats the same copying code a
 * bunch of times, but I'm trying to avoid having too many function calls in an
 * ISR. The ReadPressure code is way too much already I think.
 * 
 * This is a 1st-order low-pass Chebyshev filter with 7Hz cut-off frequency.
 * Parameters:
 * ---------------
 * none 
 * Returns:
 * --------------
 *  none
 */
void PBoard::filterStep(void)
{
   int i;

	// read pressures from sensors
   for (i = 0; i < NO_VALVES; i++) {
	   pressures[1][i] = channels[i].sensor.ReadPressure();
   }
	// smooth them out
	for (i = 0; i < NO_VALVES; i++) {
        pressures[0][i] = FILTER_ALPHA * pressures[1][i] 
                            + COMPLEMENT_ALPHA * pressures[0][i];
    }

}


/*
 * Simple proportional-integral controller to set the pressure in each channel through
 * PWM.
 * Parameters:
 * ---------------
 *  channel_no - number of channel for which we're setting the pressure
 *  pressure - pressure setpoint
 * Returns:
 * --------------
 *  none
 */
void PBoard::pressureControlStep()
{
    int i;
    float output;
    static float error_integral[NO_VALVES] = {0};
    static float time_last;

    float time_now = (float) micros();
    float elapsed_time = time_now - time_last;

    //Serial.println(error_integral[3]);
    // For each valve...
    for (i = 0; i < NO_VALVES; i++) {
        if (channels[i].automatic_pressure) {
            //Serial.print("Elapsed time: ");
            //Serial.println(elapsed_time);
            //Serial.print("setpoint: ");
            //Serial.println(channels[i].pressure_setpoint);
            // Calculate error
            float error = channels[i].pressure_setpoint - pressures[0][i];
            //Serial.print("e: ");
            //Serial.print(error);
            error_integral[i] += error * elapsed_time / 1e6;
            //Serial.print(" integral e: ");
            //Serial.print(error_integral[i]);
            // Solve integral windup by clipping integral between max and min PWM
            // values
            float integral_term = error_integral[i] * 
                                  PRESSURE_CONTROLLER_GAIN_I;
            
            if (integral_term > MAX_PWM_VALVE) {
                integral_term = MAX_PWM_VALVE;
            } else if (integral_term < MIN_PWM_VALVE) {
                integral_term = MIN_PWM_VALVE;
            }
            //Serial.print(" integral term: ");
            //Serial.println(integral_term);

            
            // output = Kp * e + Ki * integral(e)
            output = PRESSURE_CONTROLLER_GAIN_P * error + integral_term;
            //Serial.print(" output, non-clipped: ");
            //Serial.println(output);
            
            if (output > MAX_PWM_VALVE) {
                output = max_pwm_absolute;
            } else if (output < MIN_PWM_VALVE) {
                output = 0.0;
            }

            //Serial.print("Output ");
            //Serial.println((int) output);
            modulate(i, (int) output);
        }
    }
    time_last = time_now; 
}

void PBoard::run(void)
{
    if (pressureFilterTime >= FILTER_TIMER_PERIOD){
       pressureFilterTime = pressureFilterTime - FILTER_TIMER_PERIOD;
       filterStep();
    }
    if (pressureControllerTime >= PRESSURE_CONTROLLER_TIMER_PERIOD) {
        pressureControllerTime = pressureControllerTime 
                                 - PRESSURE_CONTROLLER_TIMER_PERIOD;
        pressureControlStep();
    }
}




/*
 * Reads filtered pressure from a pressure sensor on one of the channels
 * Parameters:
 * ---------------
 *   channel_no - number of the channel
 * Returns:
 * --------------
 *  pressure 
 */

float PBoard::readPressureSingleFiltered(int channel_no)
{
    float pressure;
    noInterrupts(); // Disable interrupts while we're accessing the pressures
    pressure = pressures[0][channel_no]; 
    interrupts();
    return pressure;
}


/*
 * Reads filtered pressure from all the channels
 * Parameters:
 * ---------------
 *  pressures_array - array where pressures are supposed to be saved 
 * Returns:
 * --------------
 * none 
 */
void PBoard::readPressureAllFiltered(float pressures_array[])
{
    int i;

    //noInterrupts();
    for (i = 0; i < NO_VALVES; i++) {
        pressures_array[i] = pressures[0][i];
    }
    //interrupts();
}

